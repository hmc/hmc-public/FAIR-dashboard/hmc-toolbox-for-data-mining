# HMC Toolbox for Data Mining

[![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining)](https://api.reuse.software/info/codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining)

## Description

The `HMC Toolbox for Data Mining` is designed to harvest scientific literature publications by
Helmholtz centers, find associated data publications and assess their "FAIR"-ness, meaning
how findable (F), accessible (A), interoperable (I) and reusable (R) they are.

See "The FAIR Guiding Principles for scientific data management and stewardship" at
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/ for more information.

The metadata of the literature publications is obtained chiefly from OAI-MPH-endpoints of the
different Helmholtz centers, an exception being the HZB, the publications of which are harvested
from CSV files.

For each found literature publication that has a DOI, a request to the API of [ScholeXplorer – The
Data Interlinking Service](https://doi.org/10.1045/january2017-burton) is made and linked data
publications are obtained.

In the final step of processing for all data publications that have a DOI the software makes a
request to [F-UJI](https://doi.org/10.5281/zenodo.4063720), an automated FAIR Data Assessment Tool. Thereby
FAIR-scores are generated that inform about how FAIR these data publications are.

## Installation

The `HMC Toolbox for Data Mining` comes with an easy-to-use Docker container.
[Install Docker](https://docs.docker.com/engine/install/) as well as
[Docker Compose V2](https://docs.docker.com/compose/), run it and execute the following in your
terminal to start the container:

```
docker compose up
```

A prerequisite for using the `HMC Toolbox for Data Mining` is a
[local instance of the F-UJI Server](https://github.com/pangaea-data-publisher/fuji#docker-based-installation).
Within the Docker-Compose setup an instance of the F-UJI Server is launched.

Alternatively you can also use the `HMC Toolbox for Data Mining` without Docker. In this case please
make sure, that you have installed all dependencies. The easiest way is to simply
[install poetry](https://python-poetry.org/docs/) and then run:

```
poetry install
```

## Usage

### Usage with Docker

Simply start the Docker container as described above.

### Usage with Python

Run the application directly via Python by navigating to your
locally checked out root directory `hmc-toolbox-for-data-mining` and executing:

```
poetry run hmc-toolbox run
```

### Usage as CLI Application

The `HMC Toolbox for Data Mining` can also be used per CLI. A prerequisite is that you configure a
Poetry Environment in which you can run the CLI commands. How this is done depends on the IDE you are
using. Find [instructions for Pycharm here](https://www.jetbrains.com/help/pycharm/poetry.html).

After you can run the following in your terminal:

```
hmc-toolbox run
```

It is also possible to execute only parts of the functionality provided by the `HMC Toolbox for Data Mining`.
Run

```
hmc-toolbox --help
```

for more detailed information.

### Output Structure

After successfully running the `HMC Toolbox for Data Mining` the output will be saved in the following
way:

1. For each Helmholtz center processed an eponymous output directory will be created and zipped in
the root directory, e.g. `hmc-toolbox-for-data-mining/KIT.zip`.
2. Within you will find 2 subdirectories, one of which contains the xml files harvested from an
OAI-MPH endpoint, the other is named `output`. (In the special case of the HZB instead of the
directory with xml files you will find a file called pastalist.csv.)
3. The directory `output` again contains 2 subdirectories called  `literature` and `datasets`, both
of which contain JSON files.
4. Each JSON file in `literature` represents a metadata record of a literature publication and has
the same name as the corresponding xml file or in the case of the HZB a UUID.
5. Each JSON file in `datasets` represents a metadata record of a data publication and is named
according to the following schema:

```
[literature publication it belongs to].[index].json
```

One way of conveniently displaying the output data of the `HMC Toolbox for Data Mining` is to import
it into a database and display it using the `HMC FAIR Data Dashboard`.

## Documentation

A complete API documentation of the `HMC Toolbox for Data Mining` can be found in the repository
subdirectory `documentation` in the form of html-Files. You can simply open the index.html therein
in your browser and navigate the entire documentation from there.

## Roadmap

The project as it is, is ready for usage. However, development is continued nevertheless, as there
are many potential improvements to be made.

Some features planned are:
- Validate the relationship between a literature publication and the data publications found for it.
- Find and exploit other sources of linked data-publications.
- Find and integrate other forms of FAIR assessment than F-UJI.

## Support and Contributing

If you are interested in contributing to the project or have any questions not answered here or in
the API documentation, please contact hmc-matter@helmholtz-berlin.de.

## Disclaimer

Please note that the list of data publications obtained from data harvesting using the *[HMC Toolbox for Data Mining](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining)*, as presented in the *[HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard)* is affected by method-specific biases and is neither complete nor entirely free of falsely identified data. If you wish to reuse the data shown in this dashboard for sensitive topics such as funding mechanisms, we highly recommend a manual review of the data.

We also recommend careful interpretation of evaluation-results derived from automatized FAIR assessment. The FAIR principles are a set of high-level principles and applying them depends on the specific context such as discipline-specific aspects. There are various quantitative and qualitative methods to assess the FAIRness of data (see also [FAIRassist.org](https://fairassist.org/) but no definitive methodology (see [Wilkinson et al.](https://doi.org/10.5281/zenodo.7463421)). For this reason, different FAIR assessment tools can provide different scores for the same dataset. We may include alternate, complementary methodologies in future versions of this project. To illustrate the potentials of identifying systematic gaps with automated evaluation approaches, in this dashboard you can observe evaluation results obtained from [F-UJI](https://doi.org/10.5281/zenodo.4063720) as one selected approach. Both, the F-UJI framework and the underlying [metrics](https://doi.org/10.5281/zenodo.6461229) are subject of continuous development. The evaluation results can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but focus on machine-actionable aspects and are limited with respect to human-understandable and discipline-specific aspects of metadata. Evaluations results obtained from F-UJI can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but cannot truly assess how FAIR research data really is.


## Authors

Please find all authors of this project in the CITATION.cff in this repository.

## License

The `HMC Toolbox for Data Mining` is licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License in the LICENSE.md in this repository or at

    http://www.apache.org/licenses/LICENSE-2.0



## Support and Contribution

If you are interested in contributing to the project or have any questions not answered here or in
the documentation, please contact hmc-matter@helmholtz-berlin.de.

### Funding

This work was supported by the [Helmholtz Metadata Collaboration (HMC)](www.helmholtz-metadaten.de), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative. The project was initiated by [HMC Hub Matter](https://helmholtz-metadaten.de/de/materie/team) at [Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB)](https://ror.org/02aj13c28) and was later supported by [HMC Hub Aeronautics, Space and Transport (AST)](https://helmholtz-metadaten.de/en/aeronautics/team) at the [German Aerospace Center (DLR)](https://ror.org/04bwf3e34).

<p>
  <img alt="Logo_HMC" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/HMC/HMC_Logo_M.png?raw=true" width="300">

&nbsp;
  <img alt="Logo_HZB" src="https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg" width="300">
</p>

&nbsp;
  <img alt="Logo_DLR" src="https://www.dlr.de/en/images/2021/3/dlr-logo-black/@@images/image-2000-f39fe2583e7459827bb9413a233f63b0.jpeg" width="300">
</p>



### Acknowledgements according to [CRediT](https://credit.niso.org/):

With respect to [HMC Dashboard on Open and FAIR Data in Helmholtz](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard), the following individuals are mapped to [CRediT](https://credit.niso.org/) (alphabetical order): \

[Astrid Gilein](https://orcid.org/0000-0003-4167-360X) (AG);
[Alexander Schmidt](https://orcid.org/0009-0005-1368-6114) (AS);
[Gabriel Preuß](https://orcid.org/0000-0002-3968-2446) (GP);
[Mojeeb Rahman Sedeqi](https://orcid.org/0000-0002-9694-0122) (MRS);
[Markus Kubin](https://orcid.org/0000-0002-2209-9385) (MK);
[Oonagh Brendike-Mannix](https://orcid.org/0000-0003-0575-2853)(OBM);
[Pascal Ehlers](https://orcid.org/0009-0008-4039-7977) (PE);
Tempest Glodowski (TG);
[Vivien Serve](https://orcid.org/0000-0001-9603-7630) (VS);

Contributions according to [CRediT](credit.niso.org/) are: \
[Conceptualization](https://credit.niso.org/contributor-roles/conceptualization/): AG, AS, GP, MRS, MK, OBM, VS;
[Data curation](https://credit.niso.org/contributor-roles/data-curation/): AG, AS, GP, MK, PE, TG;
[Methodology](https://credit.niso.org/contributor-roles/methodology/); AG, AS, GP, MRS, MK;
[Project administration](https://credit.niso.org/contributor-roles/project-administration/): MK;
[Software](https://credit.niso.org/contributor-roles/software/): AG, GP, PE, MRS, MK;
[Supervision](https://credit.niso.org/contributor-roles/supervision/): MK, OBM;
[Validation](https://credit.niso.org/contributor-roles/validation/): GP, MRS, PE, MK, VS;
[Visualization](https://credit.niso.org/contributor-roles/visualization/): MRS, MK, VS;
[Writing – original draft](https://credit.niso.org/contributor-roles/writing-original-draft/): MRS, MK, OBM;
[Writing – review & editing](https://credit.niso.org/contributor-roles/writing-review-editing/): MRS, MK, OBM;
