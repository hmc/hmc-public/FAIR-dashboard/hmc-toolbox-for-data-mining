"""A Unit Test for the OAIHarvester"""
import tempfile
import unittest
from pathlib import Path

from toolbox.harvester import SickleHarvester


class TestEndpoints(unittest.TestCase):
    """A Unit Test for the SickleHarvester"""
    url = 'https://verfassungsblog.de/oai/repository/'
    md_schema = 'oai_dc'
    file_name = 'oai_verfassungsblog.de_80820.oai_dc.xml'
    test_file = Path(__file__).with_name('TestData') / 'SickleHarvester' / file_name

    def test_successful_sickle_harvest(self):
        """Test the SickleHarvester"""
        with tempfile.TemporaryDirectory() as output_path:
            output_path = Path(output_path)

            SickleHarvester.harvest(
                self.url,
                output_path,
                from_date='2020-01-01',
                md_prefix=self.md_schema,
                limit=1,
            )

            with open(
                output_path / self.md_schema / self.file_name, encoding='utf-8'
            ) as downloaded_file:
                with open(self.test_file, encoding='utf-8') as expected_file:
                    self.assertEqual(downloaded_file.read(), expected_file.read())
