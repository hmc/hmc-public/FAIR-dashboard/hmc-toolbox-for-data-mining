"""A class that tests the FujiScorer"""
import json as js
import unittest
from pathlib import Path
from unittest.mock import patch

from requests import JSONDecodeError
from requests.exceptions import ConnectionError as ConnectError
from requests.exceptions import ConnectTimeout
from requests.exceptions import ReadTimeout
from requests_mock import Mocker

from toolbox.data_enricher.fair_meter.fuji_scorer import FUJI_API
from toolbox.data_enricher.fair_meter.fuji_scorer import FujiScorer
from toolbox.data_enricher.fair_meter.fuji_scorer import RETRY_WAITING_TIME
from toolbox.type_definitions import DataMetadata


class TestFujiScorer(unittest.TestCase):
    """Test class for FujiScorer"""
    def setUp(self):
        """sets up mock object for normal functioning response"""
        self.path = Path(__file__).with_name("fuji_request_mock.json")

        with open(self.path, encoding='utf-8') as file:
            self.response = js.load(file)

        self.scorer = FujiScorer()
        self.data_pid = "10.5281/zenodo.259703"
        self.data_pid_type = "doi"

    def test_get_data(self) -> None:
        """tests get_fuji_score function"""

        with Mocker() as mock:
            mock.post(FUJI_API, json=self.response)
            data = self.scorer.get_fuji_score(self.data_pid, self.data_pid_type)

        # asserts method gives correct answers
        self.assertEqual(data["pid"], self.data_pid)
        self.assertEqual(data["pid_type"], self.data_pid_type)
        self.assertEqual(
            data["metric_specification"], "https://doi.org/10.5281/zenodo.6461229"
        )
        self.assertEqual(data["maturity_A"], 2)

    @patch('time.sleep')
    def test_retry(self, sleep):
        """tests if get_fuji_score function fails specific exceptions and tries to connect again"""
        # stops normal mock object and instead uses a mock object that gives back 4 Exception and then the answer
        with Mocker() as mock:
            answers = [
                {
                    'exc': ReadTimeout
                }, {
                    'exc': ConnectTimeout
                }, {
                    'exc': ConnectError
                }, {
                    'exc': JSONDecodeError("No Json Object", js.dumps(self.response), 1)
                }, {
                    'json': self.response
                }
            ]

            mock.post(FUJI_API, answers)

            # calls get_fuji_score function
            answer = self.scorer.get_fuji_score(self.data_pid, self.data_pid_type)
            # asserts Function was called 5 times, with the right sleep time on the last call and gets an answer
            self.assertEqual(mock.call_count, 5)
            self.assertEqual(mock.request_history[4].timeout, 2200)
        sleep.assert_called_with(4 * RETRY_WAITING_TIME)
        self.assertEqual(answer["pid"], self.data_pid)

    @patch('time.sleep')
    def test_fail(self, sleep):
        """tests if get_fuji_score function fails with an exceptions and stops retrying"""
        # stops normal mock object and instead uses a mock object that gives back Exceptions
        with Mocker() as mock:
            mock.post(FUJI_API, exc=ReadTimeout)
            answer = self.scorer.get_fuji_score(self.data_pid, self.data_pid_type)

            # asserts Function was called 5 times, with the right timeout on the last call and gets an answer
            self.assertEqual(mock.call_count, 11)
            self.assertEqual(mock.request_history[10].timeout, 4000)

        sleep.assert_called_with(10 * RETRY_WAITING_TIME)
        self.assertIsNone(answer)

    def test_add_data(self):
        """tests add_fuji_scores function"""
        # generate entries for data_pubs in add_fuji_scores
        entry1 = DataMetadata()
        entry1.DataPID = "10.5281/zenodo.259703"
        entry1.DataPIDType = "doi"

        entry2 = DataMetadata()
        entry2.DataPID = "test"
        entry2.DataPIDType = "doi"

        # fill data_pubs
        data_pubs = [entry1, entry2]

        with Mocker() as mock:
            mock.post(FUJI_API, json=self.response)
            answer = self.scorer.add_fuji_scores(data_pubs)

            # asserts mock was called for each data entry
            self.assertEqual(mock.call_count, len(data_pubs))

            data_pubs[0].FAIRScores = self.scorer.get_fuji_score(
                data_pubs[0].DataPID, data_pubs[0].DataPIDType
            )
            data_pubs[1].FAIRScores = self.scorer.get_fuji_score(
                data_pubs[1].DataPID, data_pubs[1].DataPIDType
            )

        # asserts add_fuji_scores gives back the right answer
        self.assertEqual(answer, data_pubs)
