"""a class that tests the DataCiteRequester class"""
import json as js
import unittest
from pathlib import Path
from unittest.mock import patch

import requests_mock
from requests import ConnectTimeout
from requests import JSONDecodeError
from requests import ReadTimeout
from requests.exceptions import ConnectionError as ConnectError

from toolbox.requester.datacite_requester import DataCiteRequester


class TestDataCite(unittest.TestCase):
    """Test class for Datacite Requester"""
    def setUp(self):
        self.path = Path(__file__).with_name("datacite_request_mock.json")
        self.test_doi = '10.5281/zenodo.259703'

        with open(self.path, encoding='utf-8') as file:
            self.response = js.load(file)

    def test_requester(self):
        """tests if the send_datacite_request function has the right response"""
        with requests_mock.Mocker() as m:
            m.get(
                'https://api.datacite.org/dois/10.5281/zenodo.259703', json=self.response
            )

            answer = DataCiteRequester(self.test_doi).response
            self.assertEqual(m.call_count, 1)
        self.assertEqual(answer, self.response)

    @patch('time.sleep')
    def test_requester_retry(self, sleep):
        """tests if send_datacite_request function fails and retries"""
        with requests_mock.Mocker() as fail:
            fail.get(
                'https://api.datacite.org/dois/10.5281/zenodo.259703', [
                    {
                        'exc': ReadTimeout
                    }, {
                        'exc': ConnectTimeout
                    }, {
                        'exc': ConnectError
                    }, {
                        'exc':
                            JSONDecodeError("No Json Object", js.dumps(self.response), 1)
                    }, {
                        'json': self.response
                    }
                ]
            )
            answer = DataCiteRequester(self.test_doi).response
            self.assertEqual(fail.call_count, 5)
            self.assertEqual(fail.request_history[4].timeout, 300)

        self.assertEqual(answer, self.response)
        sleep.assert_called_with(240)
        self.assertEqual(sleep.call_count, 4)

    @patch('time.sleep')
    def test_requester_fail(self, sleep):
        """  tests if send_datacite_request function fails and stops retrying after 5 Retries"""
        with requests_mock.Mocker() as fail:
            fail.get(
                'https://api.datacite.org/dois/10.5281/zenodo.259703', exc=ReadTimeout
            )
            answer = DataCiteRequester(self.test_doi).response
            self.assertEqual(fail.call_count, 6)
            self.assertEqual(fail.request_history[5].timeout, 360)
        self.assertEqual(answer, {})
        sleep.assert_called_with(300)
        self.assertEqual(sleep.call_count, 5)
