"""A Unit Test for the OAIHarvester"""
import tempfile
import unittest
from pathlib import Path

import pytest
import yaml

from toolbox.harvester import SickleHarvester


class TestEndpoints(unittest.TestCase):
    """A Unit Test for testing the Endpoints"""
    centers_conf: Path = Path(__file__).parents[2] / 'centers.yaml'

    with open(centers_conf, 'r', encoding='utf8') as file:
        centers = yaml.safe_load(file)

    def template_test_successful_oai_harvest(self, center: str):
        """Test the OAI-PMH endpoint of a Helmholtz center."""
        url = self.centers[center]['endpoint']
        from_date = '2020-01-01'

        testdata_dir = Path(__file__).with_name('TestData') / center
        for md_schema in testdata_dir.iterdir():
            test_file = [f for f in md_schema.iterdir() if f.is_file()][0]
            with tempfile.TemporaryDirectory() as tmp_path:
                output_path = Path(tmp_path)
                SickleHarvester.harvest(
                    url,
                    output_path,
                    from_date=from_date,
                    md_prefix=md_schema.name,
                    limit=1,
                )
                with open(
                    output_path / md_schema.name / test_file.name,
                    encoding='utf-8',
                ) as downloaded_file:
                    with open(test_file, encoding='utf-8') as expected_file:
                        self.assertEqual(downloaded_file.read(), expected_file.read())

    def test_awi_endpoint(self):
        """Test the OAI-PMH endpoint of the AWI."""
        self.template_test_successful_oai_harvest('AWI')

    def test_cispa_endpoint(self):
        """Test the OAI-PMH endpoint of the CISPA."""
        self.template_test_successful_oai_harvest('CISPA')

    def test_desy_endpoint(self):
        """Test the OAI-PMH endpoint of the DESY."""
        self.template_test_successful_oai_harvest('DESY')

    def test_dlr_endpoint(self):
        """Test the OAI-PMH endpoint of the DLR."""
        self.template_test_successful_oai_harvest('DLR')

    def test_dzne_endpoint(self):
        """Test the OAI-PMH endpoint of the DZNE."""
        self.template_test_successful_oai_harvest('DZNE')

    def test_fzj_endpoint(self):
        """Test the OAI-PMH endpoint of the FZJ."""
        self.template_test_successful_oai_harvest('FZJ')

    def test_geomar_endpoint(self):
        """Test the OAI-PMH endpoint of the GEOMAR."""
        self.template_test_successful_oai_harvest('GEOMAR')

    def test_gfz_endpoint(self):
        """Test the OAI-PMH endpoint of the GFZ."""
        self.template_test_successful_oai_harvest('GFZ')

    def test_gsi_endpoint(self):
        """Test the OAI-PMH endpoint of the GSI."""
        self.template_test_successful_oai_harvest('GSI')

    @pytest.mark.xfail
    def test_hmgu_endpoint(self):
        """Test the OAI-PMH endpoint of the HMGU."""
        self.template_test_successful_oai_harvest('HMGU')

    def test_hzb_endpoint(self):
        """Test the OAI-PMH endpoint of the HZB."""
        self.template_test_successful_oai_harvest('HZB')

    def test_hzdr_endpoint(self):
        """Test the OAI-PMH endpoint of the HZDR."""
        self.template_test_successful_oai_harvest('HZDR')

    def test_hzi_endpoint(self):
        """Test the OAI-PMH endpoint of the HZI."""
        self.template_test_successful_oai_harvest('HZI')

    def test_kit_endpoint(self):
        """Test the OAI-PMH endpoint of the KIT."""
        self.template_test_successful_oai_harvest('KIT')

    def test_mdc_endpoint(self):
        """Test the OAI-PMH endpoint of the MDC."""
        self.template_test_successful_oai_harvest('MDC')

    def test_ufz_endpoint(self):
        """Test the OAI-PMH endpoint of the UFZ."""
        self.template_test_successful_oai_harvest('UFZ')
