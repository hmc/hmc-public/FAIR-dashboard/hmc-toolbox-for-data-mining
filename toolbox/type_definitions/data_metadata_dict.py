"""A dictionary representing a data publication."""
from typing import Optional

from .abstract_metadata_dict import AbstractMetadata


class RelatedIdentifier(AbstractMetadata):
    """A dictionary representing a related identifier."""
    relatedIdentifier: Optional[str] = None
    relationType: Optional[str] = None
    resourceTypeGeneral: Optional[str] = None
    relatedIdentifierType: Optional[str] = None


class Identifier(AbstractMetadata):
    """a datacite identifier."""
    ID: Optional[str] = None
    IDScheme: Optional[str] = None
    IDURL: Optional[str] = None


class RelationshipType(AbstractMetadata):
    """Contains all information about a data's relationship"""
    Name: Optional[str] = None
    SubType: Optional[str] = None
    SubTypeSchema: Optional[str] = None
    RelatedLitPID: Identifier = Identifier()


class DataMetadata(AbstractMetadata):
    """A dictionary representing a data publication."""
    Title: Optional[str] = None
    Creators: Optional[list[str]] = []
    Publishers: list[str] = []
    PublicationDate: Optional[str] = None
    Identifier: list[Identifier] = []
    DataPID: Optional[str] = None
    DataPIDType: Optional[str] = None
    DataInternalID: Optional[str] = None
    DataInternalIDType: Optional[str] = None
    ResourceType: Optional[str] = None
    RelatedIdentifiers: list[RelatedIdentifier] = []
    LitPID: Optional[str] = None
    LitPIDType: Optional[str] = None
    RelationshipTypes: list[RelationshipType] = []
    FAIRScores: Optional[dict] = None
