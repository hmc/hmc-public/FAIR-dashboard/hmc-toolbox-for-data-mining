"""A dictionary representing a literature publication."""
from typing import Optional

from .abstract_metadata_dict import AbstractMetadata


class LitMetadata(AbstractMetadata):
    """A dictionary representing a literature publication."""
    LitPublicationYear: Optional[str] = None
    LitAuthors: Optional[list[str]] = None
    LitTitle: Optional[str] = None
    LitJournal: Optional[str] = None
    LitPublisher: list[str] = []
    LitPID: Optional[str] = None
    LitPIDType: Optional[str] = None
    LitInternalID: Optional[str] = None
    LitInternalIDType: Optional[str] = None
    LitHelmholtzCenter: Optional[str] = None
    HelmholtzResearchField: Optional[str] = None
    HelmholtzResearchSubField: Optional[str] = None
    HelmholtzLargeScaleFacilities: list[str] = []
    HelmholtzInstruments: list[str] = []
