"""A package containing type definitions for publication metadata objects."""
from .abstract_metadata_dict import AbstractMetadata
from .data_metadata_dict import DataMetadata
from .lit_metadata_dict import LitMetadata
