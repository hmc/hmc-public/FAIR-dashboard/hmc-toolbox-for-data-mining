"""An abstract dictionary of publication metadata."""
from abc import ABC
from typing import Any
from typing import Generator
from typing import Optional


class AbstractMetadata(ABC):
    """An abstract dictionary of publication metadata."""
    LastUpdated: Optional[str] = None
    Validated: bool = False

    def __str__(self):
        # pylint: disable=not-callable
        return str(self.__dict__())

    def __dict__(self):
        return_dict = {}

        for key, attr in self._loop_attributes():
            if isinstance(attr, AbstractMetadata):
                return_dict[key] = str(attr)
            elif isinstance(attr, list):
                _list = []
                for _val in attr:
                    _list.append(
                        str(_val) if isinstance(_val, AbstractMetadata) else _val
                    )

                return_dict[key] = _list
            else:
                return_dict[key] = attr

        return return_dict

    def __eq__(self, other) -> bool:
        if not isinstance(other, type(self)):
            return False

        for key, attr in self._loop_attributes():
            if attr != getattr(other, key):
                return False

        return True

    def _loop_attributes(self) -> Generator[tuple[str, Any], None, None]:
        for key in dir(self):
            attr = getattr(self, key)

            if key.startswith('_') or callable(attr):
                continue

            yield key, attr
