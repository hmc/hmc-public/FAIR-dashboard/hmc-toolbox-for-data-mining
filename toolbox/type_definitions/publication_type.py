"""An enum class representing publication types"""
from enum import Enum


class PublicationType(Enum):
    """An enum class representing publication types"""
    LITERATURE = 'Literature'
    DATASET = 'Dataset'
