"""
A collection of constants used by the class FujiScorer.
"""
import os

FUJI_HOST = os.environ.get('FUJI_HOST', 'localhost')
FUJI_PORT = os.environ.get('FUJI_PORT', '1071')
FUJI_PROTOCOL = os.environ.get('FUJI_PROTOCOL', 'http')
FUJI_API = f"{FUJI_PROTOCOL}://{FUJI_HOST}:{FUJI_PORT}/fuji/api/v1/evaluate"

REQ_DICT = {'test_debug': True, 'use_datacite': True}

HEADERS = {
    'accept': 'application/json',
    'Authorization': 'Basic bWFydmVsOndvbmRlcndvbWFu',
    'Content-Type': 'application/json'
}

INITIAL_TIMEOUT = 1000
EXTRA_TIMEOUT = 300
RETRY_WAITING_TIME = 60
MAX_RETRY_COUNTER = 10
