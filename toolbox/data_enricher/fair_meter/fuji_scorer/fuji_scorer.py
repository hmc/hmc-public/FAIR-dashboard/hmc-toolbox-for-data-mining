"""
A class to get a F.A.I.R.-assessment (Fairness, Accessibility, Interoperability, Reusability) from
the F-UJI-Server for a given data-publication.
"""
import logging
from typing import Optional

from .fuji_conf import EXTRA_TIMEOUT
from .fuji_conf import FUJI_API
from .fuji_conf import HEADERS
from .fuji_conf import INITIAL_TIMEOUT
from .fuji_conf import MAX_RETRY_COUNTER
from .fuji_conf import REQ_DICT
from .fuji_conf import RETRY_WAITING_TIME
from toolbox.type_definitions import DataMetadata
from toolbox.utils.toolbox_request import ToolboxRequest


class FujiScorer:
    """
    A class to get a F.A.I.R.-assessment (Fairness, Accessibility, Interoperability, Reusability)
    from the F-UJI-Server for a given data-publication.
    """
    def add_fuji_scores(
        self,
        data_pubs: list[DataMetadata],
    ) -> list[DataMetadata]:
        """
        Add the fuji score to each data publication contained in the input list, if it has a DOI.
        :param data_pubs: a list of dictionaries representing data publications
        :return: the input list, enriched with the FAir-scores obtained from the F-UJI API.
        """
        for data_pub in data_pubs:
            if data_pub.DataPID and data_pub.DataPIDType:
                data_pub.FAIRScores = self.get_fuji_score(
                    data_pub.DataPID, data_pub.DataPIDType
                )

        return data_pubs

    def get_fuji_score(
        self,
        data_pid: str,
        data_pid_type: str,
    ) -> Optional[dict]:
        """
        Assess the F.A.I.R.-ness of a data-publication via the F-UJI server and return the resulting
        F-UJI scores in a dictionary.
        :param data_pid: the PID of a data-publication
        :param data_pid_type: the PID-type, e.g. DOI or HANDLE
        :return: a dictionary of F-UJI metrics for the data-publication.
        """
        json = REQ_DICT | {'object_identifier': data_pid}
        logging.debug("FUJI scorer started for data_pid %s", data_pid)
        request = ToolboxRequest(
            initial_timeout=INITIAL_TIMEOUT,
            extra_timeout=EXTRA_TIMEOUT,
            retry_waiting_time=RETRY_WAITING_TIME,
            max_retry_counter=MAX_RETRY_COUNTER,
        )

        fuji_response = request.post(FUJI_API, json=json, headers=HEADERS)

        return self._select_fuji_metrics(fuji_response, data_pid, data_pid_type)

    @staticmethod
    def _select_fuji_metrics(rs_json: dict, pid: str, pid_type: str) -> Optional[dict]:
        """
        Select important information from F-UJI response.
        :param rs_json: the JSON got as a response from F-UJI
        :param pid: the PID of the data-publication
        :param pid: the PID-type, e.g. DOI or HANDLE
        :return:
        """
        try:
            list_metric = {
                'pid': pid,
                'pid_type': pid_type,
                'metric_specification': rs_json['metric_specification'],
                'timestamp': rs_json['end_timestamp']
            }

            for score in rs_json['summary']:
                for subscore in rs_json['summary'][score]:
                    metric = score + '_' + subscore
                    metric_score = rs_json['summary'][score][subscore]
                    list_metric[metric] = float(metric_score)

            return list_metric

        except KeyError:
            return None
