"""A package to assess the F.A.I.R.-ness of data-publications via the F-UJI Tool."""
from .fuji_conf import *
from .fuji_scorer import FujiScorer
