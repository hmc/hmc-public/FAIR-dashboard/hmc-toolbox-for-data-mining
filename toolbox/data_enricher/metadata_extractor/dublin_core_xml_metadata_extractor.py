"""
Extract metadata from xml files having dublin core schema
"""
import logging
import re
from datetime import datetime
from pathlib import Path
from typing import Optional

import typer
from lxml import etree

from .abstract_metadata_extractor import AbstractMetadataExtractor
from toolbox.type_definitions import LitMetadata
from toolbox.utils.mapping import get_mapped_research_fields


class DublinCoreExtractor(AbstractMetadataExtractor):
    """A metadata-extractor for xml-files."""
    app = typer.Typer()

    namespace: dict

    class FileParseError(Exception):
        """Raised when parsing of the xml file fails."""

    # overriding abstract method
    @app.command()
    def extract(
        self,
        record: Path,
        center: Optional[str] = None,
        **_kwargs,
    ) -> LitMetadata:
        """
        Collect the publication data of a Helmholtz center from xml-files
        harvested via oaiharvest into a dataframe.

        \b
        :param record: a path to a xml-file containing a publication record in dublin core schema
        :param center: the Helmholtz center
        :return: a typed dictionary (LitMetadataDict)
        """
        with open(record, "r", encoding="utf-8") as file:
            content = file.read()

        try:
            xml = etree.fromstring(content)
        except etree.XMLSyntaxError as error:
            logging.error('Error: %s for file %s', str(error), record)
            raise self.FileParseError

        self.namespace = {
            'dc': xml.nsmap.get('dc', 'http://purl.org/dc/elements/1.1/'),
            "oa": xml.nsmap.get(None, "http://www.openarchives.org/OAI/2.0/")
        }
        pub_year = self._get_publication_year(
            xml.xpath('.//dc:date', namespaces=self.namespace)
        )

        title_list = self._get_values_from_dc_tag('.//dc:title', xml)
        oai_identifier = self._get_identifier(xml, record)

        xml_metadata = LitMetadata()
        xml_metadata.LitPublicationYear = pub_year
        xml_metadata.LitAuthors = self._get_values_from_dc_tag('.//dc:creator', xml)
        xml_metadata.LitTitle = title_list[0] if title_list else None
        xml_metadata.LitPublisher = self._get_values_from_dc_tag(
            './/dc:publisher',
            xml,
        ) or []
        xml_metadata.LitPID, xml_metadata.LitPIDType = self._parse_pid_from_xml(xml)
        xml_metadata.LitInternalID = oai_identifier
        xml_metadata.LitInternalIDType = 'oaiharvest'
        xml_metadata.LitHelmholtzCenter = center
        xml_metadata.HelmholtzResearchField = self._get_research_field(
            center, xml, oai_identifier
        )
        xml_metadata.LastUpdated = datetime.today().strftime('%Y-%m-%d')

        return xml_metadata

    def _get_identifier(self, xml: etree, record: Path) -> str:
        """
        Getting the identifier
        :param xml: is the xml
        :return: identifier which were found
        """
        for child in xml.iterfind('.//oa:identifier', namespaces=self.namespace):
            return child.text

        logging.error('OAI Record with no identifier found: %s', record)
        raise self.FileParseError

    def _get_specs(self, xml: etree) -> list[str]:
        """
        Getting the setSpec
        :param xml: is the xml
        :return: all setSpecs which were found
        """
        specs = []

        for child in xml.iterfind('.//oa:setSpec', namespaces=self.namespace):
            specs.append(child.text)

        return specs

    def _get_dc_identifier_from_metadata(self, xml: etree) -> list[str]:
        """
        Getting the identifier at dc
        :param xml: is the complete xml tree
        :return: all identifier which were found
        """
        identifier = []

        for elem in xml.iterfind('.//dc:identifier', namespaces=self.namespace):
            identifier.append(elem.text)

        return identifier

    @staticmethod
    def _get_year(date: str) -> Optional[str]:
        """Parse a year from a date."""
        year = re.search(r'\d{4}', date)
        return None if year is None else year.group()

    def _get_publication_year(
        self,
        date_tags: list[etree.ElementBase],
    ) -> Optional[str]:
        """
        Get the publication year from a list of date tags
        :param date_tags: a list of dc:date tags
        :return: a string or None
        """
        years = []

        for date_tag in date_tags:
            date = date_tag.text
            year = self._get_year(date) if date else None
            years += [year] if year else []

        return min(years) if years else None

    def _get_values_from_dc_tag(
        self,
        tag_name: str,
        xml: etree,
    ) -> Optional[list[str]]:
        """
        Get data from a dc-tag as a list of strings. If no valid value can be parsed, None is returned.
        :param tag_name: the dc-tag
        :param xml: a xml from a dc xml file
        :return: a list of strings or None
        """
        values = []

        for elem in xml.iterfind(tag_name, namespaces=self.namespace):
            values += [elem.text] if elem.text else []

        return values if values else None

    def _parse_pid_from_xml(self, xml: etree) -> tuple[Optional[str], Optional[str]]:
        """
        Parse a PID, and it's type from a xml-string if possible
        :param xml: xml from da dc_xml file
        :return: the PID and the PID type
        """
        pid_priority = ['doi', 'hdl']
        pid_list: dict[str, str] = {}

        for doi_tag in xml.iterfind('.//dc:doi', namespaces=self.namespace):
            if doi_tag.text is not None:
                return doi_tag.text.lower(), 'doi'

        for identifier_tag in xml.iterfind('.//dc:identifier', namespaces=self.namespace):
            pid, pid_type = self._parse_pid_from_string(identifier_tag.text) \
                if identifier_tag.text else (None, None)
            if pid and pid_type:
                pid_list = pid_list | {pid_type.lower(): pid.lower()}

        for relation_tag in xml.iterfind('.//dc:relation', namespaces=self.namespace):
            pid, pid_type = self._parse_pid_from_string(relation_tag.text) \
                if relation_tag.text and 'info:eu-repo/semantics/altIdentifier' in relation_tag.text else (None, None)
            if pid and pid_type and pid_type not in pid_list:
                pid_list = pid_list | {pid_type.lower(): pid.lower()}

        for relation_tag in xml.iterfind('.//dc:relation', namespaces=self.namespace):
            pid, pid_type = self._parse_pid_from_string(relation_tag.text) \
                if relation_tag.text and 'info:eu-repo/semantics/' not in relation_tag.text else (None, None)
            # dc:relation in general is pretty weak, so just use it if no other pid was found before
            if pid and pid_type and not pid_list:
                pid_list = pid_list | {pid_type.lower(): pid.lower()}

        for pid_type in pid_priority:
            if pid_type in pid_list:
                return pid_list[pid_type], pid_type

        return None, None

    @staticmethod
    def _parse_pid_from_string(pid_str: str) -> tuple[Optional[str], Optional[str]]:
        """
        Parse a PID from a string.
        :param pid_str: the input string
        :return: doi, hdl or none
        """
        pid_str_list = []
        # if we have an url with parameters we need to split it first
        if re.search(r'^https?://.*\?', pid_str):
            split_result = pid_str.split('?', 1)
            url = split_result[0]
            url_params = split_result[1]
            pid_str_list += [url]

            for param in url_params.split('&'):
                for value in param.split('=', 1):
                    pid_str_list += [value]

        else:
            pid_str_list = [pid_str]

        for pid_string in pid_str_list:
            match = re.search(r'10[.]\d{3,}(?:[.]\d+)*/\S+', pid_string)
            doi = match.group() if match else None

            if doi:
                return doi, 'doi'

            match = re.search(r'(hdl.handle.net/|hdl:)(\S+/\S+)', pid_string)
            hdl = match.group(2) if match else None

            if hdl:
                return hdl, 'hdl'

        return None, None

    def _get_research_field(
        self,
        center: Optional[str],
        xml: etree,
        oai_identifier: str,
    ) -> Optional[str]:
        """
        Check if research field exists in research field mapping, when we got HZB
        :param center: research centers
        :param xml: XML content
        :param oai_identifier: the oai_identifier of the xml
        :return: a research field or none if not contained in mapping
        """

        pub_year = self._get_publication_year(
            xml.xpath('.//dc:date', namespaces=self.namespace)
        )

        if center == "HZB":
            research_fields = get_mapped_research_fields(self._get_specs(xml))

            if len(research_fields) >= 2:
                logging.debug(
                    'Found more than one research field in %s: %s',
                    oai_identifier,
                    research_fields,
                )

            # we don't want to pick "other" or "unknown" if it exists here
            research_fields = [
                rf for rf in research_fields if rf not in ["Other", "Unknown"]
            ]

            return research_fields[0] if research_fields else None

        if center == "GFZ" and pub_year and int(pub_year) >= 2021:
            return "Earth and Environment"

        return None
