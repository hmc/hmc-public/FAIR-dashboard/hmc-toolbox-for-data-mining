"""
An abstract class representing a metadata extractor that parses the results of harvesting.
"""
from abc import ABC
from abc import abstractmethod
from typing import Any

from toolbox.type_definitions import LitMetadata


class AbstractMetadataExtractor(ABC):
    """
    An abstract class representing a metadata extractor that parses the results of harvesting.
    """
    @abstractmethod
    def extract(self, record: Any, **kwargs) -> LitMetadata:
        """
        Collect the metadata from a bunch of publications (e.g. for a defined period from a specific
        Helmholtz center) and store it in a dataframe object.
        :param record: an object, file or path representing a publication record
        :return: a typed dictionary (LitMetadataDict)
        """
