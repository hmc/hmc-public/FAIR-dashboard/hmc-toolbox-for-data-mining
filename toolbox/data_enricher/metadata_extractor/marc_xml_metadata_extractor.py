"""
Extract metadata from xml files having dublin core schema
"""
import logging
import re
from datetime import datetime
from pathlib import Path
from typing import Optional

import typer
from lxml import etree
from pymarc import parse_xml_to_array
from pymarc import Record as PyMarcRecord

from .abstract_metadata_extractor import AbstractMetadataExtractor
from toolbox.type_definitions import LitMetadata
from toolbox.utils.mapping import get_mapped_research_fields


class MarcXMLExtractor(AbstractMetadataExtractor):
    """
    A metadata-extractor for xml-files with schema MARC21.
    A definition of the data fields can be found under
    https://www.loc.gov/marc/MARC_2012_Concise_PDF/Part3_Bibliographic.pdf
    """
    app = typer.Typer()

    namespace: dict

    class FileParseError(Exception):
        """Raised when parsing of the xml file fails."""

    # overriding abstract method
    @app.command()
    def extract(
        self,
        record: Path,
        center: Optional[str] = None,
        **_kwargs,
    ) -> LitMetadata:
        """
        Collect the publication data of a Helmholtz center from xml-files
        harvested via oaiharvest into a dataframe.

        \b
        :param record: a path to a xml-file containing a publication record
        :param center: the center from which the data was harvested
        :return: a typed dictionary (LitMetadataDict)
        """
        with open(record, "r", encoding="utf-8") as file:
            content = file.read()

            try:
                xml = etree.fromstring(content)
                marc_xml: PyMarcRecord = parse_xml_to_array(record)[0]
            except AttributeError as error:
                logging.error('Error: %s for file %s', str(error), record)
                raise self.FileParseError

        self.namespace = {
            "oa": xml.nsmap.get(None, "http://www.openarchives.org/OAI/2.0/")
        }

        pid, pid_type = self._get_pid(marc_xml)

        xml_metadata = LitMetadata()
        xml_metadata.LitPublicationYear = self._get_pubyear(marc_xml)
        xml_metadata.LitAuthors = self._get_authors(marc_xml)
        xml_metadata.LitTitle = marc_xml.title
        xml_metadata.LitJournal = self._get_journal(marc_xml)
        xml_metadata.LitPublisher = [marc_xml.publisher] if marc_xml.publisher else []
        xml_metadata.LitPID = pid
        xml_metadata.LitPIDType = pid_type
        xml_metadata.LitInternalID = self._get_identifier(xml, record)
        xml_metadata.LitInternalIDType = 'oaiharvest'
        xml_metadata.LitHelmholtzCenter = center
        xml_metadata.HelmholtzResearchField = self._get_research_field(marc_xml)
        xml_metadata.HelmholtzResearchSubField = self._get_research_subfield(marc_xml)
        xml_metadata.HelmholtzLargeScaleFacilities = self._get_large_scale_facilities(
            marc_xml
        )

        xml_metadata.HelmholtzInstruments = self._get_instruments(marc_xml)
        xml_metadata.LastUpdated = datetime.today().strftime('%Y-%m-%d')

        return xml_metadata

    def _get_identifier(self, content: etree, record: Path) -> str:
        """
        Getting the identifier
        :param content: is the xml
        :return: identifier which were found
        """
        for child in content.iterfind('.//oa:identifier', namespaces=self.namespace):
            return child.text

        logging.error('OAI Record with no identifier found: %s', record)
        raise self.FileParseError

    @staticmethod
    def _get_pubyear(record: PyMarcRecord) -> Optional[str]:
        """
        Get the publication year
        :param record: xml file
        :return: year of publication
        """

        match = re.search(r'\d{4}', record.pubyear or '')
        return match.group() if match else None

    @staticmethod
    def _get_authors(record: PyMarcRecord) -> list[str]:
        """
        Get the authors of the record
        First author tag: 100, code: a; et al. tag: 700, code: a
        :param record: xml file
        :return: list of authors
        """
        authors = []

        for field in record.get_fields('100', '700'):
            authors += field.get_subfields('a')

        return authors

    def _get_pid(self, record: PyMarcRecord) -> tuple[Optional[str], Optional[str]]:
        """
        Get the doi of the literature publication.
        Check tag 024 and 773, code: "a" as a fallback
        :param record: xml file
        :return: doi or hdl or none
        """
        pid_priority = ['doi', 'hdl']
        pid_list: dict[str, str] = {}

        for field in record.get_fields('024'):
            try:
                if doi := self._get_doi_from_string(field['a']):
                    pid_list = pid_list | {'doi': doi}

                if hdl := self._get_hdl_from_string(field['a']):
                    pid_list = pid_list | {'hdl': hdl}

            except KeyError:
                pass

        for field in record.get_fields('773'):
            try:
                if 'doi' not in pid_list and (
                    doi := self._get_doi_from_string(field['a'])
                ):
                    pid_list = pid_list | {'doi': doi}

                if 'hdl' not in pid_list and (
                    hdl := self._get_hdl_from_string(field['a'])
                ):
                    pid_list = pid_list | {'hdl': hdl}

            except KeyError:
                pass

        for pid_type in pid_priority:
            if pid_type in pid_list:
                return pid_list[pid_type].lower(), pid_type

        return None, None

    @staticmethod
    def _get_doi_from_string(doi_str: str) -> Optional[str]:
        """
        Check if a given string is a doi.
        :param doi_str: string to check
        :return: none or doi
        """
        match = re.search(r'10[.]\d{3,}(?:[.]\d+)*/\S+', doi_str)
        return match.group() if match else None

    @staticmethod
    def _get_hdl_from_string(hdl_str: str) -> Optional[str]:
        """
        Check if a given string is a handle.
        :param hdl_str: string to check
        :return: none or handle
        """
        match = re.search(r'(hdl.handle.net/|hdl:)(\S+/\S+)', hdl_str)
        return match.group(2) if match else None

    @staticmethod
    def _get_research_field(record: PyMarcRecord) -> Optional[str]:
        """
        Get the research subfield of the literature publication
        (tag: 913, ind1: 1, ind2: ' ', code: l)
        :param record: xml file
        :return: a research field as string or None
        """
        research_fields: list[str] = []

        for field in record.get_fields('913'):
            try:
                research_fields.append(field['b'])
            except KeyError:
                pass

        research_fields = get_mapped_research_fields(research_fields)

        if len(research_fields) >= 2:
            logging.debug(
                'Found more than one research field in %s: %s',
                record,
                research_fields,
            )

        # we don't want to pick "other" or "unknown" if it exists here
        research_fields = [rf for rf in research_fields if rf not in ["Other", "Unknown"]]

        return research_fields[0] if research_fields else None

    @staticmethod
    def _get_research_subfield(record: PyMarcRecord) -> Optional[str]:
        """
        Get the research subfield of the literature publication
        (tag: 913, ind1: 1, ind2: ' ', code: l)
        """
        for field in record.get_fields('913'):
            try:
                return field['l']
            except KeyError:
                pass

        return None

    @staticmethod
    def _get_journal(record: PyMarcRecord) -> Optional[str]:
        """
        Get the journal of the publication
        :param record: xml file
        :return: journal
        """

        for field in record.get_fields('773'):
            try:
                return field['t']
            except KeyError:
                pass

        return None

    @staticmethod
    def _get_large_scale_facilities(record: PyMarcRecord) -> list[str]:
        """
        Get the large scale facilities of the literature publication
        (tag: 693, code: a)
        :param record: xml file
        :return: list of facilities
        """
        large_scale_facilities = []

        for field in record.get_fields('693'):
            large_scale_facilities += field.get_subfields('a')

        return list(dict.fromkeys(large_scale_facilities))

    @staticmethod
    def _get_instruments(record: PyMarcRecord) -> list[str]:
        """
        Get the large scale facilities of the literature publication
        Get the instruments of the literature publication
        (tag: 693, code: a)
        :param record: xml file
        :return: list of instruments
        """
        instruments = []

        for field in record.get_fields('693'):
            instruments += field.get_subfields('f')

        return list(dict.fromkeys(instruments))
