"""a class that combines scholex and datacite validation"""
import logging
from typing import Optional

from toolbox.requester.crossref_requester import CrossrefRequester
from toolbox.requester.datacite_requester import DataCiteRequester
from toolbox.requester.doi_org_requester import DoiOrgRequester
from toolbox.requester.scholexplorer_request import ScholexplorerRequest
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata
from toolbox.type_definitions.publication_type import PublicationType
from toolbox.utils.mapping import map_data_publishers


class LitPubValidator:
    """
    A class to check if lit odr dataset
    """
    def __init__(self, pub: LitMetadata):
        """
        inits the scholex and datacite request
        :param pub: a lit pub to be validated
        """
        self._lit_pub = pub
        self._doi_registered = False
        self._pub_found = False
        self._response_scholex: Optional[ScholexplorerRequest] = None
        self._response_datacite: Optional[DataCiteRequester] = None
        self._response_crossref: Optional[CrossrefRequester] = None
        self._publication_type: PublicationType = self._get_publication_type()

    @property
    def response_datacite(self) -> DataCiteRequester:
        """Datacite response. Takes care a request ist done before the response is needed
        :return: a Datacite response
        """
        if self._response_datacite is None:
            self._response_datacite = DataCiteRequester(self._lit_pub.LitPID or '')

        return self._response_datacite

    @property
    def validated_publication_type(self) -> Optional[PublicationType]:
        """returns the publication type if found at datacite or crossref"""
        return (
            self.response_datacite.publication_type or
            self.response_crossref.publication_type or
            self.response_scholix.publication_type
        )

    @property
    def response_scholix(self) -> ScholexplorerRequest:
        """Scholex response. Takes care a request ist done before the response is needed
        :return: a scholix response
        """
        if self._response_scholex is None:
            self._response_scholex = ScholexplorerRequest(self._lit_pub.LitPID or '')

        return self._response_scholex

    @property
    def response_crossref(self) -> CrossrefRequester:
        """Crossref response. Takes care a request ist done before the response is needed
        :return: a crossref response
        """
        if self._response_crossref is None:
            self._response_crossref = CrossrefRequester(self._lit_pub.LitPID or "")

        return self._response_crossref

    def _get_publication_type(self) -> PublicationType:
        """
        Returns the determinate publication type of the publication or None otherwise
        """
        if self._lit_pub.LitPIDType == 'doi':
            if publication_type := self.validated_publication_type:
                self._doi_registered = True
            elif doi_registrar := DoiOrgRequester(self._lit_pub.LitPID or
                                                  '').get_registrar():
                self._doi_registered = True
                logging.info(
                    "Found doi registrar %s for doi %s",
                    doi_registrar,
                    self._lit_pub.LitPID,
                )
        else:
            publication_type = self.response_scholix.publication_type

        if publication_type:
            self._pub_found = True
            return publication_type

        return PublicationType.LITERATURE

    @property
    def doi_not_registered(self) -> bool:
        """Returns true if we have found the doi at a registrar otherwise false"""
        return self._lit_pub.LitPIDType == 'doi' and not self._doi_registered

    def is_dataset(self) -> bool:
        """
        Check if data pub
        :return: true if it is or false if not
        """
        return self._publication_type == PublicationType.DATASET

    def is_validated(self) -> bool:
        """Checks if we have a validated publication type"""
        return self._pub_found

    def convert(self) -> DataMetadata:
        """
        Converts a lit pub to datapub
        :return: a datapub
        """
        data_metadata = DataMetadata()
        data_metadata.Title = self._lit_pub.LitTitle
        data_metadata.Creators = self._lit_pub.LitAuthors
        data_metadata.PublicationDate = self._lit_pub.LitPublicationYear
        data_metadata.DataPID = self._lit_pub.LitPID
        data_metadata.DataPIDType = self._lit_pub.LitPIDType
        data_metadata.DataInternalID = self._lit_pub.LitInternalID
        data_metadata.DataInternalIDType = self._lit_pub.LitInternalIDType
        data_metadata.LastUpdated = self._lit_pub.LastUpdated
        data_metadata.Publishers = map_data_publishers(self._lit_pub.LitPublisher)
        data_metadata.Validated = self._lit_pub.Validated

        self.response_datacite.add_info_to_datapub(data_metadata)

        return data_metadata
