"""
A class to get related (i.e. linked) data-publications from the DOI of a literature publication.
Presently the only source from which metadata on published datasets is pulled
is the website Scholexplorer which is requested via API. The response comes as
JSON and is further processed and returned as a special type of dictionary.
"""
import logging
from datetime import datetime
from typing import Any
from typing import Optional

from toolbox.requester.datacite_requester import DataCiteRequester
from toolbox.requester.scholexplorer_request import ScholexplorerRequest
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions.data_metadata_dict import Identifier
from toolbox.type_definitions.data_metadata_dict import RelationshipType
from toolbox.utils.mapping import map_data_publishers


class LinkedDataFinder:
    """
    A class to get linked data-publications from the DOI of a literature publication.
    """
    def find_linked_data_publications(
        self,
        lit_pid: str,
        lit_pid_type: str = 'doi',
        scholix_data: Optional[ScholexplorerRequest] = None
    ) -> list[DataMetadata]:
        """
        Request the Scholexplorer API for dataset publications that are supplements (or otherwise
        linked) to a literature publication. Process the response and return a list of objects
        representing found data_publications. Add additional info via a request to the DataCite API if possible.
        :param lit_pid: a pid for a literature publication
        :param lit_pid_type: the pid type of the pid in lit_pid
        :param scholix_data If already requested scholix data can be submitted here
        :return: a list of dictionaries representing each a data publication
        """
        if scholix_data is None:
            scholix_data = ScholexplorerRequest(lit_pid)

        data_pubs = scholix_data.get_data_pubs()

        if not data_pubs:
            logging.debug("No data_pubs found for lit_pid %s", lit_pid)

        data_metadata = [
            self._to_data_metadata_dict(data_pub, lit_pid, lit_pid_type)
            for data_pub in data_pubs
        ]

        return self._add_datacite_info(self._merge_same_datasets(data_metadata))

    @staticmethod
    def _merge_same_datasets(datasets: list[DataMetadata]) -> list[DataMetadata]:
        """
        Merges datasets if they are the same
        :param datasets: list of datasets
        :return: list without duplicates
        """
        merged_datasets: list[DataMetadata] = []

        for dataset in datasets:
            found_dataset = next(
                (
                    ds for ds in merged_datasets if ds.DataPID == dataset.DataPID and
                    ds.DataPIDType == dataset.DataPIDType
                ), None
            )

            if found_dataset:
                found_dataset.RelationshipTypes += dataset.RelationshipTypes
                continue

            merged_datasets.append(dataset)

        return merged_datasets

    def _to_data_metadata_dict(
        self, dataset: dict[str, Any], lit_pid: str, lit_pid_type: str
    ) -> DataMetadata:
        """
        Convert a dictionary to a DataMetadataDict.
        :param dataset: a dictionary with values form the Scholexplorer response.
        :param lit_pid: the DOI of the literature publication the dataset belongs to
        :return: an object representing a data publication
        """
        data_metadata = DataMetadata()
        data_metadata.Title = dataset['Title']
        data_metadata.Creators = self._get_creators(dataset)
        data_metadata.Publishers = self._get_publishers(dataset)
        data_metadata.PublicationDate = dataset['PublicationDate']
        data_metadata.Identifier = self._get_identifier(dataset)
        data_metadata.DataPID, data_metadata.DataPIDType = self._get_data_pid(dataset)
        data_metadata.LitPID = lit_pid
        data_metadata.LitPIDType = lit_pid_type
        data_metadata.RelationshipTypes = [
            self._get_relationship_type(dataset, lit_pid, lit_pid_type)
        ]
        data_metadata.FAIRScores = None
        data_metadata.LastUpdated = datetime.today().strftime('%Y-%m-%d')

        return data_metadata

    @staticmethod
    def _get_relationship_type(
        dataset: dict[str, Any], lit_pid: str, lit_pid_type: str
    ) -> RelationshipType:
        """
        get the relationship type of the dataset to a literature
        :param dataset: Date of publication
        :param lit_pid: pid of a lit publication
        :param lit_pid_type: type of the pid
        :return: RelationshipType
        """
        identifier = Identifier()
        identifier.ID = lit_pid
        identifier.IDScheme = lit_pid_type

        rel_type = RelationshipType()
        rel_type.Name = dataset['RelationshipType']['Name']
        rel_type.SubType = dataset['RelationshipType']['SubType']
        rel_type.SubTypeSchema = dataset['RelationshipType']['SubTypeSchema']
        rel_type.RelatedLitPID = identifier

        return rel_type

    @staticmethod
    def _get_creators(dataset: dict[str, Any]) -> list[str]:
        """
        Get the dataset creators.
        :param dataset: a dictionary representing a dataset
        :return: a list of the creators
        """
        creators = []

        for creator in dataset['Creator']:
            try:
                creators.append(creator['Name'])
            except KeyError:
                try:
                    creators.append(creator['name'])
                except KeyError:
                    pass

        return creators

    def _get_identifier(self, dataset: dict[str, Any]) -> list[Identifier]:
        """
        Get the dataset identifier.
        :param dataset: a dictionary representing a dataset
        :return: a list of the identifier
        """

        identifier_list: list[Identifier] = []
        avoid_duplicates: dict[str, Optional[str]] = {}

        for ident_from_data in dataset['Identifier']:
            new_ident = Identifier()
            new_ident.ID = ident_from_data['ID']
            new_ident.IDScheme = ident_from_data['IDScheme']

            if 'IDURL' in ident_from_data:
                new_ident.IDURL = ident_from_data['IDURL']

            if (ident_hash := f"{new_ident.ID}-{new_ident.IDScheme}") in avoid_duplicates:
                if avoid_duplicates[ident_hash] is None and new_ident.IDURL:
                    self._update_identifier_url(new_ident, identifier_list)
                elif new_ident.IDURL and avoid_duplicates[ident_hash] != new_ident.IDURL:
                    logging.debug(
                        "Got second mismatched identifier URL for identifier %s having old URL %s",
                        new_ident, avoid_duplicates[ident_hash]
                    )
            else:
                identifier_list.append(new_ident)
                avoid_duplicates[ident_hash] = new_ident.IDURL

        return identifier_list

    @staticmethod
    def _update_identifier_url(
        new_ident: Identifier,
        identifier_list: list[Identifier],
    ):
        """
        Updates the dataset identifier url.
        :param new_ident: new identifier
        :param identifier_list: list of identifier
        """
        for old_ident in identifier_list:
            if old_ident.ID == new_ident.ID and old_ident.IDScheme == new_ident.IDScheme:
                old_ident.IDURL = new_ident.IDURL

    @staticmethod
    def _get_publishers(dataset: dict[str, Any]) -> list[str]:
        """
        Get and infer the publisher names.
        :param dataset: a dictionary representing a dataset
        :return: a list of publisher-names
        """
        publishers = [publisher['name'] for publisher in dataset['Publisher']]
        publishers = map_data_publishers(publishers)
        id_schemes = [identifier['IDScheme'] for identifier in dataset['Identifier']]

        if 'pdb' in id_schemes:
            publishers.append('Protein Data Bank archive (PDB)')

        if 'ena' in id_schemes:
            publishers.append('European Nucleotide Archive (ENA)')

        if 'uniprot' in id_schemes:
            publishers.append('Universal Protein Knowledgebase (UniProt)')

        if {'genbank', 'ncbi-p', 'ncbi-n'} & set(id_schemes):
            publishers.append('National Library of Medicine (NLM)')

        return publishers

    @staticmethod
    def _extract_identifier(dataset: dict[str, Any]) -> dict[str, str]:
        """
        Extract an identifier (PDB) from a dataset and return it as a dictionary.
        :param dataset: a dictionary representing a dataset publication
        :return: a dictionary containing one or more PIDs, the type of which is the key, while the PID is the
        corresponding value.
        """
        pid_dict: dict[str, str] = {}

        # first we collect all available PIDs
        for identifier in dataset['Identifier']:
            if isinstance(identifier['IDScheme'],
                          str) and isinstance(identifier['ID'], str):
                if identifier['IDScheme'] not in pid_dict:
                    pid_dict = pid_dict | {
                        identifier['IDScheme'].lower(): identifier['ID'],
                    }

        # transform pdb to doi if none exists
        if 'pdb' in pid_dict and 'doi' not in pid_dict:
            pid_dict = pid_dict | {'doi': f"10.2210/pdb{pid_dict['pdb']}/pdb"}

        # use URLs if no other identifier exists
        for identifier in dataset['Identifier']:
            if isinstance(identifier['IDScheme'],
                          str) and isinstance(identifier['IDURL'], str):
                if identifier['IDScheme'] not in pid_dict:
                    pid_dict = pid_dict | {
                        identifier['IDScheme'].lower(): identifier['IDURL'],
                    }

        return pid_dict

    def _get_data_pid(
        self,
        dataset: dict[str, Any],
    ) -> tuple[Optional[str], Optional[str]]:
        """
        Get the DOI or if none is found, converts non-DOI-identifier to DOI if possible
        :param dataset: a dictionary representing a dataset
        :return: the DOI or None
        """
        pid_priority = ['doi', 'hdl']
        pid_list = self._extract_identifier(dataset)

        for pid_type in pid_priority:
            if pid_type in pid_list:
                return pid_list[pid_type], pid_type

        # if we don't have a pid from our priority list we return any pid we have
        for pid_type in pid_list:
            return pid_list[pid_type], pid_type

        logging.debug('No PID found for dataset %s', dataset)

        return None, None

    @staticmethod
    def _add_datacite_info(data_metadata: list[DataMetadata]) -> list[DataMetadata]:
        """
        Add additional information gathered from DataCite.
        :param data_metadata: a dictionary representing a dataset
        :return: list of datametadata
        """
        for data_pub in data_metadata:
            if data_pub.DataPID and data_pub.DataPIDType == 'doi':
                DataCiteRequester(data_pub.DataPID).add_info_to_datapub(data_pub)

        return data_metadata
