"""
A package to harvest literature-publication metadata from publications by Helmholtz centers,
look for related data-publications, assess the F.A.I.R.-scores of these and output all results
as JSON and/or CSV-files.
"""
from . import data_enricher
from . import exporter
from . import harvester
from . import toolbox
