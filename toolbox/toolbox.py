"""
The Toolbox harvests literature-publication metadata from publications by Helmholtz centers,
looks for related data-publications, assesses the F.A.I.R.-scores of these and outputs all results
as JSON and/or CSV-files.
"""
import datetime
import logging
import logging.config as logconfig
import os
import shutil
import time
from concurrent.futures import as_completed
from concurrent.futures import Future
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import Optional

import typer
import yaml
from pymysql import DatabaseError

from .data_enricher.fair_meter.fuji_scorer import FujiScorer
from .data_enricher.linked_data_finder import LinkedDataFinder
from .data_enricher.metadata_extractor import DublinCoreExtractor
from .data_enricher.metadata_extractor import MarcXMLExtractor
from .exporter import JSONExporter
from .harvester import SickleHarvester
from .type_definitions import DataMetadata
from .type_definitions import LitMetadata
from toolbox.data_enricher.publication_validator import LitPubValidator
from toolbox.exporter.db_exporter import DBExporter
from toolbox.requester.scholexplorer_request import ScholexplorerRequest

DCParseError = DublinCoreExtractor.FileParseError
MarcParseError = MarcXMLExtractor.FileParseError

log_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logging.conf')
logconfig.fileConfig(log_file_path)

app = typer.Typer()
app.add_typer(SickleHarvester.app, name='sickle-harvester')
app.add_typer(DublinCoreExtractor.app, name='dc-xml-extractor')
app.add_typer(MarcXMLExtractor.app, name='marc-xml-extractor')

try:
    max_threads = int(os.environ.get('MAX_THREADS', 1))
except ValueError:
    max_threads = 1


class ToolboxException(Exception):
    """Is raised when toolbox fails."""


class Center(dict):
    """Center dict coming from yaml file"""
    full_name: str
    endpoint: str


@app.command('run')
def main(
    output_path: Path = Path('./output'),
    from_date: Optional[str] = None,
    centers_conf: Path = Path('centers.yaml'),
    keep_sources: bool = True,
    zip_output: bool = False,
    skip_harvest: bool = False,
) -> None:
    """
    Harvest literature-publication metadata from publications by Helmholtz centers, look
    for related data-publications, assesses the F.A.I.R.-scores of these and output all results as
    JSON and/or CSV-files.

    \b
    :param: output_path: the output directory for the harvested data
    :param: from_date: the date from which onwards data are to be harvested, if not given, all
        available data are harvested
    :param: centers_conf: the config file specifying which centers to harvest and how
    :return: None
    """
    program_start = time.time()

    with open(centers_conf, 'r', encoding='utf8') as file:
        helmholtz_centers = yaml.safe_load(file)

    with ThreadPoolExecutor(max_workers=max_threads) as executor:
        futures = []
        future_center_map: dict[Future, str] = {}

        for center_acr, center in helmholtz_centers.items():
            future = executor.submit(
                harvest_center,
                center_acr=center_acr,
                center=center,
                output_path=output_path,
                from_date=from_date,
                skip_harvest=skip_harvest
            )
            futures.append(future)
            future_center_map[future] = center_acr

        for future in as_completed(futures):
            center_acr = future_center_map.pop(future)

            if future.exception():
                logging.error(
                    "Error while harvesting center %s: %s", center_acr, future.exception()
                )

            process_center(center_acr, output_path, executor, keep_sources, zip_output)

    logging.info(
        "Toolbox execution time: %s.",
        str(datetime.timedelta(seconds=time.time() - program_start))
    )


def harvest_center(
    center_acr: str,
    center: Center,
    output_path: Path,
    from_date: Optional[str],
    skip_harvest: bool,
) -> None:
    """
    Harvesting given center using SickleHarvester
    :param center_acr: center name
    :param output_path: where to save files
    :param center: center to harvest
    :param from_date: date of publication
    :param skip_harvest: harvest skip
    :return: None
    """

    if skip_harvest:
        logging.info("Skipping harvesting for center %s", center_acr)
        return

    center_path = output_path / center_acr
    logging.info("Starting harvesting for center %s", center_acr)

    SickleHarvester.harvest(center["endpoint"], center_path, from_date=from_date)

    logging.info("Harvesting done for center %s", center_acr)


def process_center(
    center_acr: str,
    output_path: Path,
    executor: ThreadPoolExecutor,
    keep_sources: bool,
    zip_output: bool,
) -> None:
    """
    Process and assess the harvested data for the centers using Linked Data Finder
    :param center_acr: center name
    :param output_path: where to save files
    :param executor: Thread
    :param keep_sources: keep the source file
    :param zip_output: compress data to zip
    :return: None
    """

    center_path = output_path / center_acr

    try:
        _process_center(center_path, center_acr, executor)
    except FileNotFoundError as error:
        logging.error("No data found for center %s. %s", center_acr, error)

    logging.info("Processing done for center %s", center_acr)

    if zip_output:
        logging.info("Zipping center folder %s", center_acr)
        shutil.make_archive(str(center_path), 'zip', center_path)

    if not keep_sources:
        logging.info("Deleting center folder %s", center_acr)
        shutil.rmtree(center_path)


def _process_center(
    center_path: Path,
    center_acr: str,
    executor: ThreadPoolExecutor,
) -> None:
    """
    Extract literature-publication metadata from xml-files, get linked data-publications, assess
    the F.A.I.R.-ness of these and return the results.
    :param center_path: the path to the xml-files
    :param center_acr: acronym of the Helmholtz center
    :return: the results as a nested dictionary
    """
    logging.info("Processing center %s", center_acr)
    schemas = os.listdir(center_path)
    extractor: DublinCoreExtractor | MarcXMLExtractor

    if 'marcxml' in schemas:
        schema_path = center_path / 'marcxml'
        extractor = MarcXMLExtractor()
    elif 'marc' in schemas:
        schema_path = center_path / 'marc'
        extractor = MarcXMLExtractor()
    elif 'oai_dc' in schemas:
        schema_path = center_path / 'oai_dc'
        extractor = DublinCoreExtractor()
    else:
        raise FileNotFoundError(
            "No oai_dc schema folder found but must exist for OAI-PMH."
        )

    records = [schema_path / str(file) for file in os.listdir(schema_path)]
    total_records = len(records)
    logging.info("Found %s xml files to process.", total_records)

    futures = []
    future_record_map: dict[Future, Path] = {}

    for record in records:
        future = executor.submit(
            process_record,
            extractor=extractor,
            record=record,
            center_acr=center_acr,
            center_path=center_path,
        )
        future_record_map[future] = record
        futures.append(future)
        # DDOS protection for Crossref

    percentage_bound = 0.1

    for index, future in enumerate(as_completed(futures)):
        record = future_record_map.pop(future)

        if future.exception():
            logging.error(
                "Error while processing record %s: %s", record, future.exception()
            )

        if index / total_records > percentage_bound:
            logging.info("%s%% xml files processed", round(percentage_bound * 100))
            percentage_bound += 0.1


def process_record(
    extractor: MarcXMLExtractor | DublinCoreExtractor,
    record: Path,
    center_acr: str,
    center_path: Path,
) -> None:
    """
    Extracts the metadata from the record and looks for data publications related to the litpub.
    Finally, everything is exported using the (given) exporters.
    :param extractor: The extractor which should be used to extract metadata from the given record
    :param record: Path to a record holding metadata for a literature publication
    :param center_path: the path to the xml-files
    :param center_acr: acronym of the Helmholtz center
    :return: None
    """
    try:
        lit_pub = extractor.extract(record, center=center_acr)
    except (DCParseError, MarcParseError):
        logging.warning("File %s could not be parsed, malformed xml", record)
        return

    if not lit_pub.LitPID:
        logging.debug('No LitPID found for literature %s', record)
        return

    logging.debug("Extracting done for record %s", record)

    fjs = FujiScorer()
    ldf = LinkedDataFinder()

    pub_val = LitPubValidator(lit_pub)
    lit_pub.Validated = pub_val.is_validated()

    if pub_val.doi_not_registered:
        logging.info(
            "Found doi %s for record %s but it's not registered anywhere",
            lit_pub.LitPID,
            record,
        )

    if pub_val.is_dataset():
        lit_pubs = ScholexplorerRequest(lit_pub.LitPID, as_target=True).get_literature()

        if not lit_pubs:
            data_pub = pub_val.convert()
            export([data_pub], record, center_path)

        for lit_pub in lit_pubs:
            data_pubs = fjs.add_fuji_scores(
                ldf.find_linked_data_publications(lit_pub.LitPID, lit_pub.LitPIDType)
            )

            export([lit_pub, *data_pubs], record, center_path)

    else:
        data_pubs = fjs.add_fuji_scores(
            ldf.find_linked_data_publications(
                lit_pub.LitPID, lit_pub.LitPIDType, pub_val.response_scholix
            )
        )

        export([lit_pub, *data_pubs], record, center_path)


def export(
    pubs: list[LitMetadata | DataMetadata],
    record: Path,
    center_path: Path,
    as_json: bool = False
):
    """
    Exports a literature publication, and it's related datasets
    :param pubs: publications to be exported
    :param record: given from process_record
    :param center_path: Path given from process_record
    :param as_json: flag for exporting data as JSON as well
    """
    try:
        DBExporter().export(pubs)
    except DatabaseError as error:
        logging.error("Error while exporting record %s to database.", record)
        logging.info("%s", error)

    if as_json:
        JSONExporter().export(pubs, center_path / 'output', record.stem)
