"""
A class to connect to crossref
"""
import logging
import random
import time
import urllib.parse

from toolbox.type_definitions.publication_type import PublicationType
from toolbox.utils.toolbox_request import ToolboxRequest


class CrossrefRequester:
    """
    A class to connect to Crossref
    """
    URL_CROSSREF: str = 'https://api.crossref.org/works/'

    def __init__(self, doi: str):
        """
        Inits the obj
        :param doi of datapub
        """
        self.doi = doi
        self.response = self._send_request()
        self.publication_type = self._get_publication_type()

    def _send_request(self):
        """
        Send a request to the Crossref API to find dataset publications that are supplements (or otherwise
        linked) to a literature publication.
        :return: a list of dictionaries, each representing a data publication
        """
        logging.debug("Starting Datacite request for lit_pid %s", self.doi)
        request_url = self.URL_CROSSREF + urllib.parse.quote(self.doi.strip())
        time.sleep(random.random())
        return ToolboxRequest().get(request_url)

    def _get_publication_type(self) -> PublicationType | None:
        """
        Gets Type from publication on datacite
        :return: returns str for validation with value given in Type
        """
        try:
            if self.response["message"]["type"] == "dataset" or self.response["message"][
                "publisher"] == "Worldwide Protein Data Bank":
                return PublicationType.DATASET

            return PublicationType.LITERATURE

        except KeyError:
            logging.debug("Nothing found at crossref for doi %s", self.doi)

        return None
