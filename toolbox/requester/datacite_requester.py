"""
A class to get metadate for a dataset publication from the DataCite API.
"""
import logging
import urllib.parse
from typing import Optional

from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions.data_metadata_dict import RelatedIdentifier
from toolbox.type_definitions.publication_type import PublicationType
from toolbox.utils.toolbox_request import ToolboxRequest


class DataCiteRequester:
    """
    A class to get metadate for a dataset publication from the DataCite API.
    """
    URL_DATACITE: str = 'https://api.datacite.org/dois/'

    def __init__(self, doi: str):
        """
        Inits the obj
        :param doi of datapub
        """
        self._doi = doi
        self.response = self._send_request()
        self.publication_type = self._get_publication_type()

    def is_literature(self) -> bool:
        """
        Checks if it is literature publication
        :return: returns true or false
        """
        return self.publication_type == PublicationType.LITERATURE

    def is_dataset(self) -> bool:
        """
        Checks if it is dataset
        :return: returns true or false
        """
        return self.publication_type == PublicationType.DATASET

    def _send_request(self) -> dict:
        """
        Send a request to the datacite API to find dataset publications that are supplements (or otherwise
        linked) to a literature publication.
        :return: a list of dictionaries, each representing a data publication
        """
        logging.debug("Starting Datacite request for lit_pid %s", self._doi)
        request_url = self.URL_DATACITE + urllib.parse.quote(self._doi.strip())

        return ToolboxRequest().get(request_url)

    def _get_publication_type(self) -> Optional[PublicationType]:
        """
        Gets Type from publication on datacite
        :return: returns str for validation with value given in Type
        """
        try:

            if self.response["data"]["attributes"]["types"]["resourceTypeGeneral"] in [
                "Book", "BookChapter", "ConferencePaper", "ConferenceProceeding",
                "DataPaper", "Dissertation", "Journal", "JournalArticle", "PeerReview",
                "Preprint", "Report", "Text"
            ]:
                return PublicationType.LITERATURE
            if self.response["data"]["attributes"]["types"]["resourceTypeGeneral"] in [
                "Dataset", "Collection"
            ]:
                return PublicationType.DATASET

        except KeyError:
            logging.debug("Nothing found at crossref for doi %s", self._doi)

        return None

    def add_info_to_datapub(self, data_pub: DataMetadata):
        """Add some information from datacite to the data pub
        """
        datacite_response = self.response
        try:
            data_pub.ResourceType = datacite_response['data']['attributes']['types'][
                'resourceTypeGeneral']
        except KeyError:
            pass

        try:
            related_identifiers = []

            for datacite_identifier in datacite_response['data']['attributes'][
                'relatedIdentifiers']:
                if 'relatedIdentifier' not in datacite_identifier:
                    continue

                rel_id = RelatedIdentifier()
                rel_id.relatedIdentifier = \
                    datacite_identifier['relatedIdentifier']

                if 'relationType' in datacite_identifier:
                    rel_id.relationType = datacite_identifier['relationType']

                if 'resourceTypeGeneral' in datacite_identifier:
                    rel_id.resourceTypeGeneral = datacite_identifier['resourceTypeGeneral'
                                                                    ]

                if 'relatedIdentifierType' in datacite_identifier:
                    rel_id.relatedIdentifierType = datacite_identifier[
                        'relatedIdentifierType']

                related_identifiers.append(rel_id)

            data_pub.RelatedIdentifiers = related_identifiers
        except KeyError:
            pass
