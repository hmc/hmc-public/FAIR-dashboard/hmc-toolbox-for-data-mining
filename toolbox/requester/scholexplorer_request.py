"""
A class to connect to Scholex
"""
import logging
import re
import urllib.parse
from datetime import datetime
from typing import Any
from typing import Optional

from toolbox.type_definitions import LitMetadata
from toolbox.type_definitions.publication_type import PublicationType
from toolbox.utils.toolbox_request import ToolboxRequest


class ScholexplorerRequest:
    """
    A class to connect to Scholex
    """
    URL_SCHOLEXPLORER: str = 'https://api.scholexplorer.openaire.eu/v2/Links'
    INITINAL_TIMEOUT: int = 60
    EXTRA_TIMEOUT: int = 120
    RETRY_WAITING_TIME: int = 60
    MAX_RETRY_COUNTER: int = 5

    def __init__(self, pid: str, as_target: bool = False):
        """
        Inits the obj
        :param pid of datapub
        """
        self._pid = pid
        self._as_target = as_target
        self.response = self._send_request(as_target)
        self.publication_type = self._get_publication_type()

    def _get_publication_type(self) -> Optional[PublicationType]:
        """
        Gets Type from publication on Scholexplorer
        :return: returns str for validation with value given in Type
        """
        try:
            if self.response['result'][0]['source']['Type'] == 'literature':
                return PublicationType.LITERATURE

            if self.response['result'][0]['source']['Type'] in ["dataset, software"]:
                return PublicationType.DATASET

            return None
        except IndexError:
            logging.debug("Scholix Response empty for PID %s", self._pid)
            return None

    def _send_request(self, as_target: bool) -> dict:
        """
        Send a request to the Scholexplorer API to find dataset publications that are supplements (or otherwise
        linked) to a literature publication.
        :param as_target should the request look for sourcePid or targetPids?
        :return: a list of dictionaries, each representing a data publication
        """
        logging.debug(
            "Starting Scholix for lit_pid %s and as_target %s", self._pid, as_target
        )

        request_url = (
            self.URL_SCHOLEXPLORER + ("?targetPid=" if as_target else "?sourcePid=") +
            urllib.parse.quote(self._pid.strip())
        )

        return ToolboxRequest().get(request_url)

    def _extract_metadata(self, scholix_dict: dict[str, Any]) -> LitMetadata:
        """
        Extracts a LitMetadata set
        :param scholix_dict the dict which were get by scholex requestor
        :return: returns a LitMetadata dict
        """
        pid = pid_type = None

        for identifier in scholix_dict['Identifier']:
            pid = identifier['ID'].lower()
            pid_type = identifier['IDScheme'].lower()

        scholix_metadta = LitMetadata()
        scholix_metadta.LitPID = pid
        scholix_metadta.LitPIDType = pid_type
        scholix_metadta.LitPublicationYear = self._get_year(
            scholix_dict['PublicationDate']
        )
        scholix_metadta.LitAuthors = self._get_authors(scholix_dict)
        scholix_metadta.LitTitle = scholix_dict['Title']
        scholix_metadta.LitPublisher = self._get_publishers(scholix_dict)
        scholix_metadta.LastUpdated = datetime.today().strftime('%Y-%m-%d')

        return scholix_metadta

    @staticmethod
    def _get_year(date: str) -> Optional[str]:
        """Parse a year from a date.
        :return: returns year
        """

        year = re.search(r'\d{4}', date)
        return None if year is None else year.group()

    @staticmethod
    def _get_authors(scholix_dict: dict[str, Any]) -> list[str]:
        """
        Searches for authors
        :return: returns list of authors
        """
        authors = []

        for creator in scholix_dict['Creator']:
            authors.append(creator['name'])

        return authors

    @staticmethod
    def _get_publishers(scholix_dict: dict[str, Any]) -> list[str]:
        """
        Searches for publishers
        :return: returns list of publishers
        """
        publishers = []

        for publisher in scholix_dict['Publisher']:
            publishers.append(publisher['name'])

        return publishers

    def get_literature(self) -> list[LitMetadata]:
        """
        When the publication is a dataset instead of literature we search for literature
        :return: returns list with multiple targets(catagorie in scholix json) as a dict
        """
        targets = []

        response = self._send_request(as_target=True)
        for results in response['result']:
            relation = results['RelationshipType']['SubType']
            pub_type = results['source']['Type']

            if relation == 'IsSupplementedBy' and pub_type == 'literature':
                targets.append(self._extract_metadata(results['source']))

        return targets

    def get_data_pubs(self) -> list[dict[str, Any]]:
        """
        Process the response of the Scholexplorer API into a list of dictionaries.
        :return: a dictionary with the found datasets
        """
        data_pubs = []

        try:
            for results in self.response['result']:
                target = results['target']

                if target['Type'] == 'dataset':
                    target['RelationshipType'] = results['RelationshipType']
                    data_pubs.append(target)

        except KeyError:
            pass

        return data_pubs
