"""
A class to connect to doi.org
"""
import logging
import urllib.parse

from toolbox.utils.toolbox_request import ToolboxRequest


class DoiOrgRequester:
    """
    A class to connect to doi.org
    """
    URL_DOI = "https://doi.org/ra/"

    def __init__(self, doi: str):
        """
        Inits the obj
        :param doi of datapub
        """
        self._doi = doi
        self._response = self._send_request()

    def _send_request(self) -> list[dict]:
        """
        Send a request to the datacite API to find dataset publications that are supplements (or otherwise
        linked) to a literature publication.
        :return: a list of dictionaries, each representing a data publication
        """
        logging.debug("Starting doi.org request for lit_pid %s", self._doi)
        request_url = self.URL_DOI + urllib.parse.quote(self._doi.strip())

        return ToolboxRequest().get(request_url)

    def get_registrar(self) -> str | None:
        """
        Returns the registrar of the instantiated doi or None if no registrar is available.
        """
        try:
            return self._response[0]["RA"]
        except LookupError:
            logging.debug("DOI declined by doi.org for doi %s", self._doi)

        return None
