"""
Collection of mapping functions.
Repositories are mapped using repository-mapping.yaml.
Research fields are mapped using research-field-mapping.yaml.
"""
import logging

import yaml


def map_data_publishers(publishers: list[str]) -> list[str]:
    """
    Map names of data publishers correctly.
    :param publishers: a list of publisher names
    :return: the publisher-list, after mapping
    """
    with open('repository-mapping.yaml', 'r', encoding='utf-8') as repo_mapping_file:
        repo_mapping = yaml.safe_load(repo_mapping_file)

    for i, publisher in enumerate(publishers):
        for repo, aliases in repo_mapping.items():
            if aliases and publisher.upper() in [alias.upper() for alias in aliases]:
                publishers[i] = repo
                break

    if 'Unknown' in publishers:
        publishers.remove('Unknown')

    return publishers


def get_mapped_research_fields(research_fields: list[str]) -> list[str]:
    """
    Maps a list of given research field strings using research-fields-mapping.yaml
    """
    with open('research-fields-mapping.yaml', 'r', encoding='utf-8') as rf_mapping_file:
        rf_mapping = yaml.safe_load(rf_mapping_file)
        mapped_rfs = []

        for research_field in research_fields:
            if (
                mapped_rf := _get_mapped_research_field(research_field, rf_mapping)
            ) and mapped_rf not in mapped_rfs:
                mapped_rfs.append(mapped_rf)

        return mapped_rfs


def _get_mapped_research_field(
    research_field: str, rf_mapping: dict[str, list[str]]
) -> str | None:
    """
    Check if a given research fields maps to a list of expected research fields.
    If yes, return it, if no, try to map it.
    If it's not mappable, inform and set to None.
    :param research_field: a given research field that should be mapped
    :param rf_mapping: an object holding research field mappings
    :return: research field if mapping contains or other if not
    """
    for rf_name, rf_terms in rf_mapping.items():
        if research_field in rf_terms:
            return rf_name

    logging.debug('Found unknown research-field: %s', research_field)
    return None
