# mypy: disable-error-code=operator
"""
Toolbox requester class which handles automatic retry after a failed request.
"""
import logging
import time
from typing import Any

import requests
from requests import JSONDecodeError
from requests.exceptions import ConnectionError as ConnectError
from requests.exceptions import ConnectTimeout
from requests.exceptions import ReadTimeout


class ToolboxRequest:
    """
    Mirrors the functionality of the famous requests library but with an automatic retry on failure.
    """
    def __init__(
        self,
        initial_timeout: int = 60,
        extra_timeout: int = 60,
        retry_waiting_time: int = 60,
        max_retry_counter: int = 5,
    ):
        self.initial_timeout = initial_timeout
        self.extra_timeout = extra_timeout
        self.retry_waiting_time = retry_waiting_time
        self.max_retry_counter = max_retry_counter

    def post(self, request_url: str, retry_count: int = 0, **kwargs) -> Any:
        """
        Send a request using post method
        """
        return self._handle_request("POST", request_url, retry_count, **kwargs)

    def get(self, request_url: str, retry_count: int = 0, **kwargs) -> Any:
        """
        Send a request using get method
        """
        return self._handle_request("GET", request_url, retry_count, **kwargs)

    def _handle_request(
        self, request_type: str, request_url: str, retry_count: int = 0, **kwargs
    ) -> Any:
        req = requests.post if request_type == "POST" else requests.get

        try:
            with req(
                request_url,
                timeout=(self.initial_timeout + self.extra_timeout * retry_count),
                **kwargs
            ) as response:
                if response.status_code == 429:
                    raise ReadTimeout

                if response.status_code == 404:
                    return {}

                return response.json()

        except KeyError as error:
            logging.error('KeyError on request: %s', error)

        except (ReadTimeout, ConnectTimeout, ConnectError, JSONDecodeError) as error:
            logging.warning(
                "Request failed for request type %s and url %s with error %s.",
                request_type, request_url, error
            )
            logging.info("kwargs: %s", kwargs)

            if retry_count < self.max_retry_counter:
                retry_count += 1
                logging.info("Retry! Counter is now %s", retry_count)
                time.sleep(self.retry_waiting_time * retry_count)

                return self._handle_request(
                    request_type, request_url, retry_count, **kwargs
                )

            logging.error(
                "Request failed after %s retries for request type %s and url %s",
                self.max_retry_counter, request_type, request_url
            )
            logging.info("kwargs: %s", kwargs)

            if isinstance(error, JSONDecodeError):
                logging.info(
                    "Got response %s", {
                        'status': response.status_code,
                        'content': response.content,
                    }
                )

        return {}
