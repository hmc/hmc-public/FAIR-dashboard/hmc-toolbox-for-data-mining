"""
Harvest xml files in Dublin Core Format with the literature publications of Helmholtz centers.
Uses Sickle library
"""
import itertools
import logging
import os
from pathlib import Path
from typing import Optional

import pathvalidate
import typer
from lxml import etree
from sickle import Sickle


class SickleHarvester:
    """Data Harvester for OAI-PMH-endpoints."""
    app = typer.Typer()

    @staticmethod
    @app.command()
    def harvest(
        url: str,
        output_path: Path,
        from_date: Optional[str] = None,
        md_prefix: Optional[str] = None,
        limit: Optional[int] = None,
    ) -> None:
        """
        Download data from an OAI-PMH endpoint going back to a specified into an output directory.
        Check which metadata schemas are available and harvest all of them into separate
        subdirectories.

        \b
        :param url: the OAI-PMH endpoint
        :param output_path: the output directory
        :param from_date: the date from which onwards data are to be harvested, if not given, all
        available data are harvested
        :param md_prefix: fixed metadata prefix to be harvested
        :param limit: an optional maximum number of records to harvest
        :return: None
        """
        md_prefixes = [md_prefix
                      ] if md_prefix else SickleHarvester._get_metadata_prefixes(url)

        for md_pf in md_prefixes:
            SickleHarvester._oai_harvest(
                url, output_path / md_pf, from_date, limit, md_pf
            )

    @staticmethod
    def _get_metadata_prefixes(url: str) -> list[str]:
        """
        Request all metadata prefixes for all available schemas from an OAI-PMH endpoint.
        :param url: the OAI-PMH endpoint
        :return: a list of metadata prefixes
        """
        metadata_prefixes = []
        sickle = Sickle(url)
        metadata_formats = sickle.ListMetadataFormats()

        for metadata_format in metadata_formats:
            metadata_prefixes.append(metadata_format.metadataPrefix)

        logging.info("Found %s schema(s) for endpoint %s", len(metadata_prefixes), url)

        # for now, we return only schemas we need
        # later one can harvest all and merge information
        if 'marcxml' in metadata_prefixes:
            return ['marcxml']

        if 'marc' in metadata_prefixes:
            return ['marc']

        # must exist for every OAI-PMH API
        return ['oai_dc']

    @staticmethod
    def _oai_harvest(
        url: str,
        output_path: Path,
        from_date: Optional[str] = None,
        limit: Optional[int] = None,
        md_prefix: str = 'oai_dc',
    ) -> None:
        """
        Harvest from OAI-PMH xml files with the literature publications of Helmholtz centers.
        :param output_path: where to put the files
        :param url: the API endpoint
        :param from_date: optional, a date in format "YYYY-MM-DD"
        :param limit: an optional maximum number of records to harvest
        :param md_prefix: metadata-prefix / xml-schema, default is 'oai_dc' (Dublin Core Format).
        :return: None
        """

        records = Sickle(endpoint=url, max_retries=5, default_retry_after=30).ListRecords(
            **{
                'metadataPrefix': md_prefix,
                'from': from_date,
                'ignore_deleted': True,
            }
        )
        logging.info(
            'Harvesting from endpoint %s with schema %s started with from date %s', url,
            md_prefix, from_date
        )

        os.makedirs(output_path, exist_ok=True)

        for record in itertools.islice(records, limit):
            file_name = pathvalidate.sanitize_filename(
                '.'.join([str(record.header.identifier), md_prefix, 'xml']), "_"
            )
            with open(output_path / file_name, 'wb') as file:
                file.write(
                    etree.tostring(record.xml, encoding="utf-8", pretty_print=True)
                )
