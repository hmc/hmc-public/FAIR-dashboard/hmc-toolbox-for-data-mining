"""
Takes a dictionary and/or a list of dictionaries and export them as a JSON file.
Optionally the files can be gzipped while exporting.
"""
import json
import os
import uuid
from pathlib import Path
from typing import Optional

import pathvalidate

from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata


class JSONExporter:
    """
    Takes a dictionary and/or a list of dictionaries and export them as a JSON file.
    Optionally the files can be gzipped while exporting.
    """
    @staticmethod
    def export(
        pubs: list[LitMetadata | DataMetadata],
        output_path: Path = Path("."),
        file_id: Optional[str] = None,
    ) -> None:
        """
        exports dicts as a JSON file
        :param pubs: publications to be exported
        :param output_path: where to save the json
        :param file_id: id for file name
        :return: None
        """

        file_id = (
            str(uuid.uuid4())
            if file_id is None else pathvalidate.sanitize_filename(file_id)
        )
        pub_path = output_path / file_id
        os.makedirs(pub_path, exist_ok=True)
        lit_pub_counter = 0
        data_pub_counter = 0

        for pub in pubs:
            if isinstance(pub, LitMetadata):
                publication_type = "lit"
                index = lit_pub_counter
                lit_pub_counter += 1
            else:
                publication_type = "data"
                index = data_pub_counter
                data_pub_counter += 1

            with open(
                pub_path / ".".join([
                    file_id,
                    publication_type,
                    str(index),
                    "json",
                ]),
                "w",
                encoding="utf-8"
            ) as outfile:
                json.dump(pub.__dict__(), outfile, indent=4)
