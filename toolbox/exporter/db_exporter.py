"""
Takes a dictionary and/or a list of dictionaries and writes them directly into a database.
"""
import logging
import os
from typing import Optional

from .db_models.mysql_connection import DbConfig
from .db_models.mysql_connection import MysqlConnection
from toolbox.exporter.db_models.exception.publication_exists_error import PublicationExistsError
from toolbox.exporter.db_models.publication_db import PublicationDb
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata


class DBExporter:
    """
    Takes a dictionary and/or a list of dictionaries and writes them directly into a database
    """
    def __init__(self, publication_model: Optional[PublicationDb] = None):
        self.publication_model = publication_model if publication_model is not None else PublicationDb(
            MysqlConnection(
                DbConfig(
                    user=os.environ.get('TOOLBOX_DB_USER', ''),
                    password=os.environ.get('TOOLBOX_DB_PASS', ''),
                    host=os.environ.get('TOOLBOX_DB_HOST', ''),
                    database=os.environ.get('TOOLBOX_DB_NAME', '')
                )
            )
        )

    def export(self, pubs: list[LitMetadata | DataMetadata]) -> None:
        """
        Write data for literature publication and associated  data publications into the database.
        :param pubs: list of the pubs
        :return: None
        """

        # we put the lit pubs first to database, so we can link the data pubs later
        for lit_pub in [pub for pub in pubs if isinstance(pub, LitMetadata)]:
            self._insert_or_update(lit_pub)

        for data_pub in [pub for pub in pubs if isinstance(pub, DataMetadata)]:
            self._insert_or_update(data_pub)

    def _insert_or_update(self, pub: LitMetadata | DataMetadata) -> None:
        try:
            logging.debug("Insert pub %s", pub)
            self.publication_model.insert(pub)
        except PublicationExistsError:
            logging.debug("Pub already exists, updating instead")
            self.publication_model.update(pub)
