"""
A class representing a connection to a MySQL database, e.g. MariaDB.
"""
import pymysql
from pymysql.cursors import DictCursor


class DbConfig:
    """Typing object for a DbConfig of Toolbox MysqlConnection"""
    user: str
    password: str
    host: str
    database: str

    def __init__(self, user: str, password: str, host: str, database: str):
        self.user = user
        self.password = password
        self.host = host
        self.database = database


class MysqlConnection:
    """
    A class representing a connection to a MySQL database, e.g. MariaDB.
    """
    def __init__(self, db_config: DbConfig):
        """Connect to Database."""
        self.connection = pymysql.connect(
            user=db_config.user,
            password=db_config.password,
            host=db_config.host,
            database=db_config.database,
            cursorclass=DictCursor,
            charset="utf8mb4",
        )
