"""
A module representing operations on the table reference in the Database.
"""
from typing import Optional

from pymysql.cursors import DictCursor

from toolbox.type_definitions.data_metadata_dict import RelationshipType


def insert(
    cursor: DictCursor,
    lit_id: int,
    data_id: int,
    rel_type: RelationshipType,
) -> None:
    """
    Inserts a relationship between a literature- and a data-publication.
    """
    cursor.execute(
        """
        INSERT INTO reference (
            reference_id,
            reference_to_id,
            type,
            sub_type,
            sub_type_schema
        ) VALUES (
            %(reference_id)s,
            %(reference_to_id)s,
            %(type)s,
            %(sub_type)s,
            %(sub_type_schema)s
          )""", {
            'reference_id': lit_id,
            'reference_to_id': data_id,
            'type': rel_type.Name,
            'sub_type': rel_type.SubType,
            'sub_type_schema': rel_type.SubTypeSchema,
        }
    )


def update(
    cursor: DictCursor,
    lit_id: int,
    data_id: int,
    rel_type: RelationshipType,
) -> Optional[int]:
    """
    Inserts a relationship between a literature- and a data-publication.
    """
    return cursor.execute(
        """
        UPDATE reference
        SET
            type = %(type)s,
            sub_type = %(sub_type)s,
            sub_type_schema = %(sub_type_schema)s
        WHERE
            reference_id = %(reference_id)s
          AND
            reference_to_id = %(reference_to_id)s
        """, {
            'reference_id': lit_id,
            'reference_to_id': data_id,
            'type': rel_type.Name,
            'sub_type': rel_type.SubType,
            'sub_type_schema': rel_type.SubTypeSchema,
        }
    )
