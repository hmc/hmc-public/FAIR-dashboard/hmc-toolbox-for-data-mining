"""
A module representing operations on the table publication in the Database.
"""
import logging
from typing import Optional

from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.config import DB_STRING_SEPERATOR
from toolbox.exporter.db_models.config import UNKNOWN_FIELD_CONTENT
from toolbox.exporter.db_models.exception.publication_mismatch_error import PublicationMismatchError
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata


def select(
    cursor: DictCursor,
    pub_id: int,
) -> tuple:
    """
    Get the pid-type for a publication by its PID from the table 'publication_identifier' by its id.
    """
    select_lit_pub_data_by_pub_id_query = """
        SELECT title, journal, publisher_list, publication_date,
        internal_id, internal_id_type, last_updated
        FROM publication
        WHERE id = %s
    """
    cursor.execute(select_lit_pub_data_by_pub_id_query, pub_id)

    if result := cursor.fetchone():
        return tuple(result.values())

    return (None, ) * 7


def insert(cursor: DictCursor, pub: LitMetadata | DataMetadata) -> int:
    """
    Insert a data-publication into the DB table 'publication'.
    """

    if isinstance(pub, LitMetadata):
        publishers = pub.LitPublisher or [UNKNOWN_FIELD_CONTENT]

        pub_data = {
            'type': 'Literature' if pub.Validated else UNKNOWN_FIELD_CONTENT,
            'title': pub.LitTitle,
            'journal': pub.LitJournal,
            'publisher_deprecated': publishers[0],
            'publisher_list': DB_STRING_SEPERATOR.join(publishers),
            'publication_date': pub.LitPublicationYear,
            'publication_year': pub.LitPublicationYear,
            'internal_id': pub.LitInternalID,
            'internal_id_type': pub.LitInternalIDType,
            'last_updated': pub.LastUpdated,
        }
    else:
        publishers = pub.Publishers or [UNKNOWN_FIELD_CONTENT]
        pub_data = {
            'type': 'Dataset',
            'title': pub.Title,
            'publisher_deprecated': publishers[0],
            'publisher_list': DB_STRING_SEPERATOR.join(publishers),
            'publication_date': pub.PublicationDate,
            'publication_year': pub.PublicationDate[0:4] if pub.PublicationDate else None,
            'internal_id': pub.DataInternalID,
            'internal_id_type': pub.DataInternalIDType,
            'last_updated': pub.LastUpdated,
        }

    cursor.execute(
        f"""
        INSERT INTO publication
        ({','.join(pub_data.keys())})
        VALUES ({','.join(f"%({key})s" for key in pub_data)})
        """,
        pub_data,
    )

    return cursor.lastrowid


def update(
    cursor: DictCursor,
    db_pub: LitMetadata | DataMetadata,
    new_pub: LitMetadata | DataMetadata,
):
    """
    Updates the data for a given publication in the database table 'publication'.
    Data from `new_pub` is taken to overwrite existing ones.
    """
    pub_id = publication_identifier.get_by_publication(cursor, db_pub)
    logging.debug('UPDATING publication table for lit pub with pid %s', pub_id)

    if isinstance(db_pub, LitMetadata) and isinstance(new_pub, LitMetadata):
        old_pub_data, new_pub_data = _data_for_lit_pub_update(db_pub, new_pub)
    elif isinstance(db_pub, DataMetadata) and isinstance(new_pub, DataMetadata):
        old_pub_data, new_pub_data = _data_for_data_pub_update(db_pub, new_pub)
    else:
        raise PublicationMismatchError

    new_pub_data = new_pub_data | {'pub_id': pub_id}
    logging.debug('Replacing current data %s with %s', old_pub_data, new_pub_data)

    cursor.execute(
        """
        UPDATE publication
        SET
        `title` = %(title)s,
        `journal` = %(journal)s,
        `publisher_deprecated` = %(publisher_deprecated)s,
        `publication_year` = %(publication_year)s,
        `last_updated` = CURDATE(),
        `publication_date` = %(publication_date)s,
        `publisher_list` = %(publisher_list)s
        WHERE id = %(pub_id)s
    """, new_pub_data
    )


def _data_for_lit_pub_update(
    db_pub: LitMetadata,
    new_pub: LitMetadata,
) -> tuple[dict, dict]:
    publisher_deprecated, publisher_list = _get_publisher_data(
        db_pub.LitPublisher, new_pub.LitPublisher
    )
    publication_date = new_pub.LitPublicationYear or db_pub.LitPublicationYear

    old_pub_data = {
        'title': db_pub.LitTitle,
        'journal': db_pub.LitJournal,
        'publisher_deprecated': db_pub.LitPublisher[0] if db_pub.LitPublisher else None,
        'publication_year': db_pub.LitPublicationYear,
        'publication_date': db_pub.LitPublicationYear,
        'publisher_list': DB_STRING_SEPERATOR.join(db_pub.LitPublisher),
    }

    new_pub_data = {
        'title': new_pub.LitTitle or db_pub.LitTitle,
        'journal': new_pub.LitJournal or db_pub.LitJournal,
        'publisher_deprecated': publisher_deprecated,
        'publication_year': publication_date[0:4] if publication_date else None,
        'publication_date': publication_date,
        'publisher_list': publisher_list,
    }

    return old_pub_data, new_pub_data


def _data_for_data_pub_update(
    db_pub: DataMetadata,
    new_pub: DataMetadata,
) -> tuple[dict, dict]:
    publisher_deprecated, publisher_list = _get_publisher_data(
        db_pub.Publishers, new_pub.Publishers
    )
    publication_date = new_pub.PublicationDate or db_pub.PublicationDate

    old_pub_data = {
        'title':
            db_pub.Title,
        'publisher_deprecated':
            db_pub.Publishers[0] if db_pub.Publishers else UNKNOWN_FIELD_CONTENT,
        'publisher_list':
            db_pub.Publishers,
        'publication_date':
            db_pub.PublicationDate,
    }

    new_pub_data = {
        'title': new_pub.Title or db_pub.Title,
        'journal': None,
        'publisher_deprecated': publisher_deprecated,
        'publication_year': publication_date[0:4] if publication_date else None,
        'publication_date': publication_date,
        'publisher_list': publisher_list,
    }

    return old_pub_data, new_pub_data


def _get_publisher_data(
    old_publisher_list: Optional[list], new_publisher_list: Optional[list]
) -> tuple[str, str]:
    merged_publisher = (old_publisher_list or []).copy()
    merged_publisher.extend(
        publisher
        for publisher in (new_publisher_list or []) if publisher not in merged_publisher
    )

    if UNKNOWN_FIELD_CONTENT in merged_publisher:
        merged_publisher.remove(UNKNOWN_FIELD_CONTENT)

    if merged_publisher:
        publisher_deprecated = merged_publisher[0]
        publisher_list = DB_STRING_SEPERATOR.join(merged_publisher)
    else:
        publisher_deprecated = publisher_list = UNKNOWN_FIELD_CONTENT

    return publisher_deprecated, publisher_list
