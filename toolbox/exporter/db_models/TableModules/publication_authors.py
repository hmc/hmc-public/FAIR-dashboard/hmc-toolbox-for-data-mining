"""
A module representing operations on the table publication_authors in the Database.
"""
from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.config import DB_STRING_SEPERATOR
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata


def select_by_pub_id(
    cursor: DictCursor,
    pub_id: int,
) -> list[str]:
    """
    Get the authors / creators of a publication by their publication-id from the table
    'publication_authors'.
    """
    cursor.execute(
        """
        SELECT full_names
        FROM publication_authors
        WHERE publication_id = %s
        """,
        pub_id,
    )

    if result := cursor.fetchone():
        return result['full_names'].split(DB_STRING_SEPERATOR)

    return []


def insert(
    cursor: DictCursor,
    pub: LitMetadata | DataMetadata,
    pub_id: int,
) -> None:
    """
    Insert the authors of a publication into the database table 'publication_authors'. They are
    inserted as a comma-separated string.
    """
    if isinstance(pub, LitMetadata):
        authors = pub.LitAuthors
    else:
        authors = pub.Creators

    full_names = DB_STRING_SEPERATOR.join(authors or [])
    author_data = {'publication_id': pub_id, 'full_names': full_names}
    cursor.execute(
        """
        INSERT INTO publication_authors
        (publication_id, full_names)
        VALUES (%(publication_id)s, %(full_names)s)
        """,
        author_data,
    )


def update(
    cursor: DictCursor,
    pub: LitMetadata | DataMetadata,
    pub_id: int,
) -> None:
    """Update the table publication-authors."""
    if isinstance(pub, LitMetadata):
        authors = pub.LitAuthors or []
    else:
        authors = pub.Creators or []

    cursor.execute(
        """
        UPDATE publication_authors
        SET `full_names` = %s
        WHERE publication_id = %s
    """,
        [DB_STRING_SEPERATOR.join(authors), pub_id],
    )
