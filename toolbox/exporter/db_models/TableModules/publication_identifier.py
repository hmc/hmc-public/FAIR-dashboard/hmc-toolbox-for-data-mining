"""
A module representing operations on the table publication_identifier in the Database.
"""
import logging
from typing import Optional

from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.config import UNKNOWN_FIELD_CONTENT
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata
from toolbox.type_definitions.data_metadata_dict import Identifier
from toolbox.type_definitions.publication_type import PublicationType


def get_pub_id_by_pid(
    cursor: DictCursor,
    pid: Optional[str],
    pub_type: Optional[PublicationType] = None,
) -> Optional[int]:
    """Get the DB-id for a publication by its PID from the table 'publication_identifier'."""

    query_data = {'pid': pid, 'type': pub_type.value} if pub_type else {'pid': pid}

    cursor.execute(
        f"""
                SELECT publication_id
                FROM publication_identifier AS p_id
                JOIN publication as p
                  ON p.id = p_id.publication_id
                WHERE p_id.identifier_id = %(pid)s
                {f"AND (p.type = %(type)s OR p.type = '{UNKNOWN_FIELD_CONTENT}')" if pub_type else ''}
                """, query_data
    )

    if result := cursor.fetchone():
        return result['publication_id']

    return None


def get_by_publication(
    cursor: DictCursor,
    pub: LitMetadata | DataMetadata,
) -> Optional[int]:
    """Get the first found DB-id for a publication resp. its identifier(s)"""
    if isinstance(pub, LitMetadata):
        return get_pub_id_by_pid(cursor, pub.LitPID, PublicationType.LITERATURE)

    if isinstance(pub, DataMetadata):
        if pub_ids := get_multiple_for_publication(cursor, pub):
            return pub_ids[0]

    return None


def get_multiple_for_publication(
    cursor: DictCursor,
    pub: DataMetadata,
) -> list[int]:
    """Get the list of found DB-ids for a data publication resp. its identifier list"""
    pub_ids = []
    identifiers = pub.Identifier.copy()

    if pub.DataPID and pub.DataPIDType:
        identifier = Identifier()
        identifier.ID = pub.DataPID
        identifier.IDScheme = pub.DataPIDType
        identifier.IDURL = UNKNOWN_FIELD_CONTENT
        identifiers.append(identifier)

    for identifier in identifiers:
        if (
            pub_id := get_pub_id_by_pid(cursor, identifier.ID, PublicationType.DATASET)
        ) and pub_id not in pub_ids:
            pub_ids.append(pub_id)

    return pub_ids


def get_identifier_by_pub_id(
    cursor: DictCursor,
    pub_id: int,
) -> list[Identifier]:
    """
    Get the identifier string, the identifier type (e.g. doi, handle, etc) and the
    identifier url and returns it as an Identifier object.
    """
    cursor.execute(
        """
        SELECT identifier_id, identifier_type, identifier_url
        FROM publication_identifier
        WHERE publication_id = %s
    """,
        pub_id,
    )

    identifiers = []

    for row in cursor.fetchall():
        identifier = Identifier()
        identifier.ID = row['identifier_id']
        identifier.IDScheme = row['identifier_type']
        identifier.IDURL = row['identifier_url']
        identifiers.append(identifier)

    return identifiers


def insert(
    cursor: DictCursor,
    pub: LitMetadata | DataMetadata,
    pub_id: int,
) -> None:
    """
    Insert a publication identifier into the DB table 'publication_identifier'.
    """
    pub_identifier_query = """
        INSERT INTO publication_identifier
            (publication_id, identifier_id, `identifier_type`, identifier_url)
        VALUES
            (%(publication_id)s, %(identifier_id)s, %(identifier_type)s, %(identifier_url)s)
    """

    if isinstance(pub, LitMetadata) and pub.LitPID and pub.LitPIDType:
        cursor.execute(
            pub_identifier_query, {
                'publication_id': pub_id,
                'identifier_id': pub.LitPID,
                'identifier_type': pub.LitPIDType,
                'identifier_url': None,
            }
        )

    elif isinstance(pub, DataMetadata):
        query_data = []
        found_data_pid = False

        for identifier in pub.Identifier:
            if pub.DataPID == identifier.ID \
               and pub.DataPIDType == identifier.IDScheme:
                found_data_pid = True

            query_data.append(
                {
                    'publication_id': pub_id,
                    'identifier_id': identifier.ID,
                    'identifier_type': identifier.IDScheme,
                    'identifier_url': identifier.IDURL,
                }
            )

        if not found_data_pid:
            query_data.append(
                {
                    'publication_id': pub_id,
                    'identifier_id': pub.DataPID,
                    'identifier_type': pub.DataPIDType,
                    'identifier_url': UNKNOWN_FIELD_CONTENT,
                }
            )

        cursor.executemany(pub_identifier_query, query_data)


def add(
    cursor: DictCursor,
    db_pub: DataMetadata,
    new_pub: DataMetadata,
) -> None:
    """
    Adds the identifiers from new_pub which does not exist already in db_pub to the database
    with the same publication_id as db_pub has.
    """
    pub_id = get_by_publication(cursor, db_pub)

    new_identifier: list[Identifier] = []

    for new_ident in new_pub.Identifier:
        for old_ident in db_pub.Identifier:
            if new_ident.ID == old_ident.ID and new_ident.IDScheme == old_ident.IDScheme and old_ident.IDURL:
                if new_ident.IDURL:
                    logging.debug(
                        "Got second mismatched identifier URL for identifier %s having old URL %s",
                        new_ident, old_ident.IDURL
                    )
            elif new_ident.ID == old_ident.ID and new_ident.IDScheme == new_ident.IDScheme:
                new_identifier.append(new_ident)

    query_data = []
    for identifier in new_identifier:
        query_data.append(
            {
                'publication_id': pub_id,
                'identifier_id': identifier.ID,
                'identifier_type': identifier.IDScheme,
                'identifier_url': identifier.IDURL,
            }
        )
    cursor.executemany(
        """
        INSERT INTO publication_identifier
            (publication_id, identifier_id, `identifier_type`, identifier_url)
        VALUES
            (%(publication_id)s, %(identifier_id)s, %(identifier_type)s,%(identifier_url)s)
        ON DUPLICATE KEY UPDATE
            identifier_url = %(identifier_url)s
        """, query_data
    )
