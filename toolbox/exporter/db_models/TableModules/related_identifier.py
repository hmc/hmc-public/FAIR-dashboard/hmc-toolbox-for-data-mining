"""
A module representing operations on the table related_identifier in the Database.
"""
from pymysql.cursors import DictCursor

from toolbox.type_definitions.data_metadata_dict import RelatedIdentifier


def get(
    cursor: DictCursor,
    data_pub_id: int,
) -> list[RelatedIdentifier]:
    """Gets all related identifier for the given data pub id"""
    cursor.execute(
        """
        SELECT related_identifier, relation_type, resource_type, related_identifier_type
        FROM related_identifier
        WHERE publication_id = %s
        """,
        data_pub_id,
    )

    related_identifiers = []

    for row in cursor.fetchall():
        rel_id = RelatedIdentifier()
        rel_id.relatedIdentifier = row['related_identifier']
        rel_id.relationType = row['relation_type']
        rel_id.resourceTypeGeneral = row['resource_type']
        rel_id.relatedIdentifierType = row['related_identifier_type']
        related_identifiers.append(rel_id)

    return related_identifiers


def insert(
    cursor: DictCursor,
    data_pub_id: int,
    related_identifiers: list[RelatedIdentifier],
) -> None:
    """
    Inserts the given related identifiers for the given data pub id
    """

    for rel_id in related_identifiers:
        cursor.execute(
            """
            INSERT INTO related_identifier (
                related_identifier,
                relation_type,
                resource_type,
                related_identifier_type,
                publication_id
            )
            VALUES (%s, %s, %s, %s, %s)
            """, (
                rel_id.relatedIdentifier,
                rel_id.relationType,
                rel_id.resourceTypeGeneral,
                rel_id.relatedIdentifierType,
                data_pub_id,
            )
        )


def delete(cursor: DictCursor, data_pub_id: int) -> None:
    """
    Deletes all related identifier for the given data publication id
    """
    cursor.execute(
        """
        DELETE FROM related_identifier WHERE publication_id = %s
        """,
        data_pub_id,
    )


def update(
    cursor: DictCursor,
    data_pub_id: int,
    related_identifiers: list[RelatedIdentifier],
) -> None:
    """
    Updates related identifier for the given data pub id by removing all old ones
    and inserting the given ones.
    """
    delete(cursor, data_pub_id)
    insert(cursor, data_pub_id, related_identifiers)
