"""
A module representing operations on the table fuji_score in the Database.
"""
from datetime import datetime
from typing import Optional

from pymysql.cursors import DictCursor


def insert(
    cursor: DictCursor,
    pub_id: int,
    fuji_score: dict,
) -> int:
    """Insert a F-UJI Score into the table fuji_score."""
    fuji_score['publication_id'] = pub_id
    fuji_score["timestamp"] = _format_timestamp_for_db(fuji_score["timestamp"])

    # remove fields we don't have in the database
    del fuji_score['pid']
    del fuji_score['pid_type']

    # noinspection SqlInsertValues
    cursor.execute(
        f"""
        INSERT INTO fuji_score (`{'`,`'.join(fuji_score.keys())}`)
        VALUES ({','.join(f"%({key})s" for key in fuji_score.keys())})
    """, fuji_score
    )

    return cursor.lastrowid


def get_latest(
    cursor: DictCursor, pub_id: int, pid: Optional[str], pid_type: Optional[str]
) -> Optional[dict]:
    """
    Selects the latest inserted fuji score from the database.
    """
    cursor.execute(
        """
        SELECT *
        FROM fuji_score
        WHERE publication_id = %(pub_id)s
        ORDER BY timestamp DESC
        LIMIT 1
        """,
        {'pub_id': pub_id},
    )

    if fair_scores := cursor.fetchone():
        fair_scores['timestamp'] = _format_timestamp_from_db(fair_scores['timestamp'])

        # removing some fields so that the FAIRScores object equals the one from F-UJI
        if 'id' in fair_scores:
            del fair_scores['id']

        if 'publication_id' in fair_scores:
            del fair_scores['publication_id']

        return fair_scores | {'pid': pid, 'pid_type': pid_type}

    return None


def _format_timestamp_for_db(timestamp: str) -> str:
    return ' '.join(timestamp.strip('Z').split('T'))


def _format_timestamp_from_db(date_time: datetime) -> str:
    # We need to add the Z manually here:
    # https://stackoverflow.com/questions/19654578/python-utc-datetime-objects-iso-format-doesnt-include-z-zulu-or-zero-offset
    return date_time.isoformat(sep='T', timespec='auto') + 'Z'
