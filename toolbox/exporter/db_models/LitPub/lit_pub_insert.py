"""Module for inserting a literature publication to database"""
from enum import Enum
from typing import Optional

from pymysql.constants.ER import DUP_ENTRY
from pymysql.cursors import DictCursor
from pymysql.err import IntegrityError

from toolbox.exporter.db_models.config import UNKNOWN_FIELD_CONTENT
from toolbox.exporter.db_models.exception.publication_exists_error import PublicationExistsError
from toolbox.exporter.db_models.TableModules import publication
from toolbox.exporter.db_models.TableModules import publication_authors
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.type_definitions import LitMetadata


class Table(Enum):
    """
    An enum class representing a table in a MariaDB database to which toolbox data can be exported.
    """
    CENTER = 'center'
    INST = 'instrument'
    LSF = 'large_scale_facility'
    R_FIELD = 'research_field'
    RS_FIELD = 'research_sub_field'


def insert(cursor: DictCursor, lit_pub: LitMetadata) -> int:
    """ Insert a literature publication into the database. """
    lit_pub_id = publication.insert(cursor, lit_pub)

    try:
        publication_identifier.insert(cursor, lit_pub, lit_pub_id)
    except IntegrityError as error:
        raise PublicationExistsError from error if error.args[0] == DUP_ENTRY else error

    publication_authors.insert(cursor, lit_pub, lit_pub_id)

    if lit_pub.LitHelmholtzCenter:
        _insert_helmholtz_center(cursor, lit_pub_id, lit_pub.LitHelmholtzCenter)

    _insert_research_field(
        cursor, lit_pub_id, lit_pub.HelmholtzResearchField or UNKNOWN_FIELD_CONTENT
    )

    if lit_pub.HelmholtzResearchSubField:
        _insert_research_sub_field(cursor, lit_pub_id, lit_pub.HelmholtzResearchSubField)

    if lit_pub.HelmholtzLargeScaleFacilities:
        _insert_large_scale_facility(
            cursor, lit_pub_id, lit_pub.HelmholtzLargeScaleFacilities
        )

    if lit_pub.HelmholtzInstruments:
        _insert_instrument(cursor, lit_pub_id, lit_pub.HelmholtzInstruments)

    return lit_pub_id


def _select_or_insert_by_name(
    cursor: DictCursor,
    table: Table,
    name: str,
) -> Optional[int]:
    """
    Select a field by name or if it isn't filled yet, insert the name as value.
    """
    try:
        cursor.execute(
            f"""
               INSERT INTO {table.value}
               (name, description)
               VALUES (%(name)s, '')
               """,
            {'name': name},
        )

        return cursor.lastrowid

    except IntegrityError as error:
        if error.args[0] == DUP_ENTRY:
            cursor.execute(
                f"""
                SELECT id
                FROM {table.value}
                WHERE name = %(name)s
                """,
                {'name': name},
            )

        if result := cursor.fetchone():
            return result['id']

        raise error


def _insert_helmholtz_center(
    cursor: DictCursor,
    pub_id: int,
    center: str,
) -> None:
    """
    Writes a Helmholtz center into it's proper table and into it's corresponding
    relationship table.
    """
    center_id = _select_or_insert_by_name(
        table=Table.CENTER,
        name=center,
        cursor=cursor,
    )

    cursor.execute(
        """
        INSERT INTO center_has_publication
        (center_id, publication_id)
        VALUES (%s, %s)
        """,
        (center_id, pub_id),
    )


def _insert_research_field(
    cursor: DictCursor,
    pub_id: int,
    research_field: str,
) -> None:
    """
    Writes a research field into it's proper table and into it's corresponding
    relationship table.
    """
    research_field_id = _select_or_insert_by_name(
        table=Table.R_FIELD,
        name=research_field,
        cursor=cursor,
    )
    cursor.execute(
        """
        INSERT INTO research_field_has_publication
        (research_field_id, publication_id)
        VALUES (%s, %s)
        """,
        (research_field_id, pub_id),
    )


def _insert_research_sub_field(
    cursor: DictCursor,
    pub_id: int,
    research_sub_field: str,
) -> None:
    """
    Writes a research sub-field into it's proper table and into it's corresponding
    relationship table.
    """
    research_sub_field_id = _select_or_insert_by_name(
        table=Table.RS_FIELD,
        name=research_sub_field,
        cursor=cursor,
    )
    cursor.execute(
        """
        INSERT INTO research_sub_field_has_publication
        (research_sub_field_id, publication_id)
        VALUES (%s, %s)
        """,
        (research_sub_field_id, pub_id),
    )


def _insert_large_scale_facility(
    cursor: DictCursor,
    pub_id: int,
    large_scale_facilities: list[str],
) -> None:
    """
    Writes a large_scale_facility into it's proper table and into it's corresponding
    relationship table.
    """

    for large_scale_facility in large_scale_facilities:
        large_scale_facility_id = _select_or_insert_by_name(
            table=Table.LSF,
            name=large_scale_facility,
            cursor=cursor,
        )
        cursor.execute(
            """
            INSERT INTO large_scale_facility_has_publication
            (large_scale_facility_id, publication_id)
            VALUES (%s, %s)
            """,
            (large_scale_facility_id, pub_id),
        )


def _insert_instrument(
    cursor: DictCursor,
    pub_id: int,
    instruments: list[str],
) -> None:
    """
    Writes a large_scale_facility into it's proper table and into it's corresponding
    relationship table.
    """
    for instrument in instruments:
        instrument_id = _select_or_insert_by_name(
            table=Table.INST,
            name=instrument,
            cursor=cursor,
        )
        cursor.execute(
            """
            INSERT INTO instrument_has_publication
            (instrument_id, publication_id)
            VALUES (%s, %s)
            """,
            (instrument_id, pub_id),
        )
