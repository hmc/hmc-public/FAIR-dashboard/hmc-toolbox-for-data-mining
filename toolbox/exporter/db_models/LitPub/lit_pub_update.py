"""Module for updating a literature publication in database"""
import logging

from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.exception.publication_not_found_error import PublicationNotFoundError
from toolbox.exporter.db_models.LitPub import lit_pub_get
from toolbox.exporter.db_models.TableModules import publication
from toolbox.exporter.db_models.TableModules import publication_authors
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.type_definitions import LitMetadata


def update(
    cursor: DictCursor,
    new_lit_pub: LitMetadata,
) -> None:
    """Update all data for a literature publication in the database."""
    if not (
        (db_lit_pub := lit_pub_get.get(cursor, new_lit_pub.LitPID)) and
        (pub_id := publication_identifier.get_by_publication(cursor, db_lit_pub))
    ):
        raise PublicationNotFoundError(new_lit_pub.LitPID)

    props_may_change = ['LitPublicationYear', 'LitTitle', 'LitJournal', 'LitPublisher']

    props_must_not_change = [
        'LitInternalID', 'LitInternalIDType', 'LitHelmholtzCenter',
        'HelmholtzResearchField', 'HelmholtzResearchSubField',
        'HelmholtzLargeScaleFacilities', 'HelmholtzInstruments'
    ]

    if _props_changed(
        props_may_change + ['LitAuthors'],
        db_lit_pub,
        new_lit_pub,
        ignore_none_values=True
    ) and _prop_changed('LitHelmholtzCenter', db_lit_pub, new_lit_pub):
        logging.debug(
            'Found updated data for lit pub from another Helmholtz center.'
            'Skip updating.'
            'Publication with pub_id %s read from DB: %s'
            'Updated LitMetaDataDict: %s', pub_id, db_lit_pub, new_lit_pub
        )
        return

    if _props_changed(props_may_change, db_lit_pub, new_lit_pub):
        logging.debug(
            "props may change, updating pid %s with data %s", pub_id, new_lit_pub
        )
        publication.update(cursor, db_lit_pub, new_lit_pub)

    if _prop_changed('LitAuthors', db_lit_pub, new_lit_pub):
        logging.debug(
            "LitAuthors changed, updating pid %s with data %s", pub_id, new_lit_pub
        )
        publication_authors.update(cursor, new_lit_pub, pub_id)

    if _props_changed(props_must_not_change, db_lit_pub, new_lit_pub):
        logging.debug(
            'Found updated data for lit pub although it should not. '
            'Skip updating. '
            'Publication with pub_id %s read from DB: %s'
            'Updated LitMetaDataDict: %s', pub_id, db_lit_pub, new_lit_pub
        )


def _props_changed(
    props: list[str],
    current_pub: LitMetadata,
    new_pub: LitMetadata,
    ignore_none_values: bool = False
) -> bool:
    props_changed = False
    for prop in props:
        props_changed = props_changed or _prop_changed(
            prop, current_pub, new_pub, ignore_none_values
        )

    return props_changed


def _prop_changed(
    prop: str,
    current_pub: LitMetadata,
    new_pub: LitMetadata,
    ignore_none_values: bool = False
) -> bool:
    old_prop_value = getattr(current_pub, prop)
    new_prop_value = getattr(new_pub, prop)

    if ignore_none_values and (old_prop_value is None or new_prop_value is None):
        return False

    return old_prop_value != new_prop_value
