"""Module for getting a literature publication from database"""
from typing import Optional

from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.config import DB_STRING_SEPERATOR
from toolbox.exporter.db_models.config import UNKNOWN_FIELD_CONTENT
from toolbox.exporter.db_models.TableModules import publication
from toolbox.exporter.db_models.TableModules import publication_authors
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.type_definitions import LitMetadata
from toolbox.type_definitions.publication_type import PublicationType


def get(cursor: DictCursor, pid: Optional[str]) -> Optional[LitMetadata]:
    """Get a literature publication from the database."""
    pub_id = publication_identifier.get_pub_id_by_pid(
        cursor, pid, PublicationType.LITERATURE
    )

    if pub_id is not None:
        lit_pub = LitMetadata()

        if lit_identifier := publication_identifier.get_identifier_by_pub_id(
            cursor, pub_id
        ):
            lit_pub.LitPID = lit_identifier[0].ID
            lit_pub.LitPIDType = lit_identifier[0].IDScheme

        lit_pub.LitTitle, lit_pub.LitJournal, \
            publisher_list, lit_pub.LitPublicationYear, \
            lit_pub.LitInternalID, lit_pub.LitInternalIDType, \
            lit_pub.LastUpdated = publication.select(cursor, pub_id)

        publisher = publisher_list.split(DB_STRING_SEPERATOR) if publisher_list else []

        if UNKNOWN_FIELD_CONTENT in publisher:
            publisher.remove(UNKNOWN_FIELD_CONTENT)

        lit_pub.LitPublisher = publisher
        lit_pub.LitAuthors = publication_authors.select_by_pub_id(cursor, pub_id)
        lit_pub.LitHelmholtzCenter = _get_helmholtz_center(cursor, pub_id)
        lit_pub.HelmholtzResearchField = _get_research_field(cursor, pub_id)
        lit_pub.HelmholtzResearchSubField = _get_research_sub_field(cursor, pub_id)
        lit_pub.HelmholtzLargeScaleFacilities = _get_large_scale_facilities(
            cursor, pub_id
        )
        lit_pub.HelmholtzInstruments = _get_instruments(cursor, pub_id)

        return lit_pub

    return None


def _get_helmholtz_center(cursor: DictCursor, pub_id: int) -> Optional[str]:
    cursor.execute(
        """
        SELECT c.name
        FROM center c
         JOIN center_has_publication chp
           ON c.id = chp.center_id
        WHERE chp.publication_id = %s
        """,
        pub_id,
    )

    if result := cursor.fetchone():
        return result['name']

    return None


def _get_research_field(cursor: DictCursor, pub_id: int) -> Optional[str]:
    cursor.execute(
        """
        SELECT rf.name
        FROM research_field rf
         JOIN research_field_has_publication rfhp
           ON rf.id = rfhp.research_field_id
        WHERE rfhp.publication_id = %s
        """,
        pub_id,
    )

    if result := cursor.fetchone():
        return result['name']

    return None


def _get_research_sub_field(cursor: DictCursor, pub_id: int) -> Optional[str]:
    cursor.execute(
        """
        SELECT rsf.name
        FROM research_sub_field rsf
         JOIN research_sub_field_has_publication rsfhp
           ON rsf.id = rsfhp.research_sub_field_id
        AND rsfhp.publication_id = %s
        """,
        pub_id,
    )

    if result := cursor.fetchone():
        return result['name']

    return None


def _get_large_scale_facilities(cursor: DictCursor, pub_id: int) -> list[str]:
    cursor.execute(
        """
        SELECT lsf.name
        FROM large_scale_facility lsf
         JOIN large_scale_facility_has_publication lsfhp
           ON lsf.id = lsfhp.large_scale_facility_id
        WHERE lsfhp.publication_id = %s
        """,
        pub_id,
    )

    facilities = []

    for facility in cursor.fetchall():
        facilities.append(facility['name'])

    return facilities


def _get_instruments(cursor: DictCursor, pub_id: int) -> list[str]:
    cursor.execute(
        """
        SELECT i.name
        FROM instrument i
         JOIN instrument_has_publication ihp
           ON i.id = ihp.instrument_id
        WHERE ihp.publication_id = %s
        """,
        pub_id,
    )

    instruments = []

    for instrument in cursor.fetchall():
        instruments.append(instrument['name'])

    return instruments
