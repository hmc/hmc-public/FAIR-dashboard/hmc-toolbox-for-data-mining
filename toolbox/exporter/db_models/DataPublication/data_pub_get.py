"""Module for getting a data publication from database"""
from typing import Optional

from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.config import DB_STRING_SEPERATOR
from toolbox.exporter.db_models.TableModules import fuji_score
from toolbox.exporter.db_models.TableModules import publication
from toolbox.exporter.db_models.TableModules import publication_authors
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.exporter.db_models.TableModules import related_identifier
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions.data_metadata_dict import Identifier
from toolbox.type_definitions.data_metadata_dict import RelationshipType
from toolbox.type_definitions.publication_type import PublicationType


def get(cursor: DictCursor, pid: Optional[str]) -> Optional[DataMetadata]:
    """Get a data-publication from the database."""
    if not (
        pub_id :=
        publication_identifier.get_pub_id_by_pid(cursor, pid, PublicationType.DATASET)
    ):
        return None

    data_pub = DataMetadata()
    data_pub.Identifier = publication_identifier.get_identifier_by_pub_id(cursor, pub_id)
    pid_type = None

    for identifier in data_pub.Identifier:
        if pid == identifier.ID:
            pid_type = identifier.IDScheme

    data_pub.DataPID = pid
    data_pub.DataPIDType = pid_type
    data_pub.Title, _, publisher_list, data_pub.PublicationDate, \
        data_pub.DataInternalID, data_pub.DataInternalIDType, data_pub.LastUpdated = publication.select(cursor, pub_id)
    data_pub.Publishers = publisher_list.split(DB_STRING_SEPERATOR) \
        if publisher_list else []
    data_pub.Creators = publication_authors.select_by_pub_id(cursor, pub_id)
    data_pub.RelationshipTypes = _get_relationship_types(cursor, pub_id)
    data_pub.FAIRScores = fuji_score.get_latest(cursor, pub_id, pid, pid_type)
    data_pub.RelatedIdentifiers = related_identifier.get(cursor, pub_id)

    if data_pub.RelationshipTypes:
        first_identifier = data_pub.RelationshipTypes[0].RelatedLitPID
        lit_pub_id = publication_identifier.get_pub_id_by_pid(cursor, first_identifier.ID)

        if lit_pub_id and (
            lit_identifier :=
            publication_identifier.get_identifier_by_pub_id(cursor, lit_pub_id)
        ):
            data_pub.LitPID = lit_identifier[0].ID
            data_pub.LitPIDType = lit_identifier[0].IDScheme

    return data_pub


def _get_relationship_types(
    cursor: DictCursor,
    pub_id: int,
) -> list[RelationshipType]:
    cursor.execute(
        """
        SELECT type, sub_type, sub_type_schema, identifier_id, identifier_type, identifier_url
          FROM reference r
          JOIN publication_identifier as pi
            ON r.reference_id = pi.publication_id
         WHERE r.reference_to_id = %(data_id)s
        """,
        {'data_id': pub_id},
    )

    rel_types = []

    for result in cursor.fetchall():
        identifier = Identifier()
        identifier.ID = result['identifier_id']
        identifier.IDScheme = result['identifier_type']
        identifier.IDURL = result['identifier_url']

        rel_type = RelationshipType()
        rel_type.Name = result['type']
        rel_type.SubType = result['sub_type']
        rel_type.SubTypeSchema = result['sub_type_schema']
        rel_type.RelatedLitPID = identifier

        rel_types.append(rel_type)

    return rel_types
