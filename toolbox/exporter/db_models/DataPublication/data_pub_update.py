"""Module for updating a data publication in database"""
import logging

from pymysql.cursors import DictCursor

from toolbox.exporter.db_models.DataPublication import data_pub_get
from toolbox.exporter.db_models.exception.multiple_datapubs_error import MultipleDataPubsError
from toolbox.exporter.db_models.exception.publication_not_found_error import PublicationNotFoundError
from toolbox.exporter.db_models.TableModules import fuji_score
from toolbox.exporter.db_models.TableModules import publication
from toolbox.exporter.db_models.TableModules import publication_authors
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.exporter.db_models.TableModules import reference
from toolbox.exporter.db_models.TableModules import related_identifier
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions.publication_type import PublicationType


def update(
    cursor: DictCursor,
    new_data_pub: DataMetadata,
) -> None:
    """Update all data for a data-publication in the database."""
    if (
        db_pids :=
        publication_identifier.get_multiple_for_publication(cursor, new_data_pub)
    ) and len(db_pids) > 1:
        raise MultipleDataPubsError

    if not ((db_data_pub := data_pub_get.get(cursor, new_data_pub.DataPID)) and db_pids):
        raise PublicationNotFoundError(new_data_pub.DataPID)

    pub_id = db_pids[0]

    publication_props = ['Title', 'Publishers', 'PublicationDate']

    if _props_changed(publication_props, db_data_pub, new_data_pub):
        logging.debug("publication_props changed, updating")
        publication.update(cursor, db_data_pub, new_data_pub)

    if new_data_pub.Creators and _prop_changed('Creators', db_data_pub, new_data_pub):
        logging.debug("Creators changed, updating")
        publication_authors.update(cursor, new_data_pub, pub_id)

    # Because RelatedIdentifiers are a list of dicts, we need to do some more work here
    if new_data_pub.RelatedIdentifiers and _prop_changed(
        'RelatedIdentifiers', db_data_pub, new_data_pub
    ):
        logging.debug("RelatedIdentifiers changed, updating")
        related_identifier.update(cursor, pub_id, new_data_pub.RelatedIdentifiers)

    if new_data_pub.FAIRScores and _fuji_scores_differ(db_data_pub, new_data_pub):
        logging.debug("FAIRScores changed, updating")
        fuji_score.insert(cursor, pub_id, new_data_pub.FAIRScores)

    for new_rel_type in new_data_pub.RelationshipTypes:
        rel_type_unknown = new_rel_type not in db_data_pub.RelationshipTypes
        lit_pub_id = publication_identifier.get_pub_id_by_pid(
            cursor, new_rel_type.RelatedLitPID.ID, PublicationType.LITERATURE
        )

        if rel_type_unknown and lit_pub_id:
            logging.debug(
                'Found new relation for data_pub_pid %s and lit_pub_id %s',
                pub_id,
                lit_pub_id,
            )
            reference.insert(cursor, lit_pub_id, pub_id, new_rel_type)

        elif rel_type_unknown:
            logging.warning(
                'Found new relation for data_pub_pid %s but lit pub with %s %s is unknown!',
                pub_id, new_rel_type.RelatedLitPID.IDScheme, new_rel_type.RelatedLitPID.ID
            )


def _props_changed(
    props: list[str],
    current_pub: DataMetadata,
    new_pub: DataMetadata,
    ignore_none_values: bool = False,
) -> bool:
    props_changed = False
    for prop in props:
        if props_changed := props_changed or _prop_changed(
            prop, current_pub, new_pub, ignore_none_values
        ):
            return props_changed

    return props_changed


def _prop_changed(
    prop: str,
    current_pub: DataMetadata,
    new_pub: DataMetadata,
    ignore_none_values: bool = False,
) -> bool:
    old_prop_value = getattr(current_pub, prop)
    new_prop_value = getattr(new_pub, prop)

    if ignore_none_values and (old_prop_value is None or new_prop_value is None):
        return False

    return old_prop_value != new_prop_value


def _fuji_scores_differ(data_pub1: DataMetadata, data_pub2: DataMetadata) -> bool:
    fuji_score_1 = data_pub1.FAIRScores.copy() if data_pub1.FAIRScores else None
    fuji_score_2 = data_pub2.FAIRScores.copy() if data_pub2.FAIRScores else None

    if fuji_score_1 and fuji_score_2:
        # we only want to compare the plain fuji_scores + current metric
        del fuji_score_2['timestamp']
        del fuji_score_1['timestamp']

        del fuji_score_2['pid']
        del fuji_score_1['pid']

        del fuji_score_2['pid_type']
        del fuji_score_1['pid_type']

        return fuji_score_1 != fuji_score_2

    if fuji_score_2 and fuji_score_1 is None or fuji_score_1 and fuji_score_2 is None:
        return True

    return False
