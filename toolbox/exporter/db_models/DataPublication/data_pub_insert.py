"""Module for inserting a data publication to database"""
import logging

from pymysql.constants.ER import DUP_ENTRY
from pymysql.cursors import DictCursor
from pymysql.err import IntegrityError

from toolbox.exporter.db_models.exception.publication_exists_error import PublicationExistsError
from toolbox.exporter.db_models.TableModules import fuji_score
from toolbox.exporter.db_models.TableModules import publication
from toolbox.exporter.db_models.TableModules import publication_authors
from toolbox.exporter.db_models.TableModules import publication_identifier
from toolbox.exporter.db_models.TableModules import reference
from toolbox.exporter.db_models.TableModules import related_identifier
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions.publication_type import PublicationType


def insert(cursor: DictCursor, data_pub: DataMetadata) -> int:
    """Insert a data-publication into the database."""
    data_pub_id = publication.insert(cursor, data_pub)

    try:
        publication_identifier.insert(cursor, data_pub, data_pub_id)
    except IntegrityError as error:
        raise PublicationExistsError from error if error.args[0] == DUP_ENTRY else error
    publication_authors.insert(cursor, data_pub, data_pub_id)

    if data_pub.FAIRScores:
        fuji_score.insert(cursor, data_pub_id, data_pub.FAIRScores)
        _insert_publication_metric(cursor, data_pub_id, 'Fuji Score')

    for rel_type in data_pub.RelationshipTypes:
        lit_pub_id = publication_identifier.get_pub_id_by_pid(
            cursor, rel_type.RelatedLitPID.ID, PublicationType.LITERATURE
        )

        if lit_pub_id:
            reference.insert(cursor, lit_pub_id, data_pub_id, rel_type)
        else:
            logging.debug(
                'Got data pub for related PID %s but PID not in database (yet)',
                rel_type.RelatedLitPID.ID
            )

    if data_pub.RelatedIdentifiers:
        related_identifier.insert(cursor, data_pub_id, data_pub.RelatedIdentifiers)

    return data_pub_id


def _insert_publication_metric(
    cursor: DictCursor,
    pub_id: int,
    metric_type: str,
) -> None:
    cursor.execute(
        """
        INSERT INTO metric (publication_id, type)
        VALUES (%s, %s)
        """,
        [pub_id, metric_type],
    )
