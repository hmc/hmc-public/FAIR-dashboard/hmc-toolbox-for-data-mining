"""
Raised when a new data publication contains identifiers that belong to multiple
data publication entries in the database. For now, we don't handle this (so no
merging or so) but raise this Error instead
"""


class MultipleDataPubsError(Exception):
    """
    Raised when a new data publication contains identifiers that belong to multiple
    data publication entries in the database. For now, we don't handle this (so no
    merging or so) but raise this Error instead
    """
