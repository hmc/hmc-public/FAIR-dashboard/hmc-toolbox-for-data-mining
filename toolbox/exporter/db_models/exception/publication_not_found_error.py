"""
An exception raised, when there is no Publication found for the given pid
"""
from typing import Optional


class PublicationNotFoundError(Exception):
    """
    An exception raised, when there is no Publication found for the given pid
    """
    def __init__(self, pid: Optional[str]):
        self.pid = pid
        super().__init__(f"Publication not found for pid {pid}")
