"""
An exception raised, when it is tried to insert a publication into the database, the identifier
of which already exists, meaning the publication already has an entry.
"""


class PublicationExistsError(Exception):
    """
    An exception raised, when it is tried to insert a publication into the database, the identifier
    of which already exists, meaning the publication already has an entry.
    """
