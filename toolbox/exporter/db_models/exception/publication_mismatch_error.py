"""
An exception raised, when trying to update / compare / whatever MetaDataDicts of different types
"""


class PublicationMismatchError(Exception):
    """
    An exception raised, when trying to update / compare / whatever MetaDataDicts of different types
    """
