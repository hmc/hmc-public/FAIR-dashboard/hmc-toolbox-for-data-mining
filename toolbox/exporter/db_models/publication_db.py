"""
A model class representing a literature publication in a database.
"""
from toolbox.exporter.db_models.DataPublication import data_pub_insert
from toolbox.exporter.db_models.DataPublication import data_pub_update
from toolbox.exporter.db_models.LitPub import lit_pub_insert
from toolbox.exporter.db_models.LitPub import lit_pub_update
from toolbox.exporter.db_models.mysql_connection import MysqlConnection
from toolbox.type_definitions import DataMetadata
from toolbox.type_definitions import LitMetadata

Publication = LitMetadata | DataMetadata


class PublicationDb:
    """
    A model class representing a literature publication in a database.
    """
    def __init__(self, connection: MysqlConnection):
        """Connect to Database."""
        self.connection = connection.connection

    def insert(self, pub: Publication) -> int:
        """ Insert a literature publication into the database. """
        with self.connection as connection:
            connection.ping(reconnect=True)
            connection.begin()
            with connection.cursor() as cursor:
                if isinstance(pub, LitMetadata):
                    publication_id = lit_pub_insert.insert(
                        cursor=cursor,
                        lit_pub=pub,
                    )

                else:
                    publication_id = data_pub_insert.insert(
                        cursor=cursor,
                        data_pub=pub,
                    )

            connection.commit()
        return publication_id

    def update(self, pub: Publication) -> None:
        """Update all data for a literature publication in the database."""
        with self.connection as connection:
            connection.ping(reconnect=True)
            connection.begin()
            with connection.cursor() as cursor:
                if isinstance(pub, LitMetadata):
                    lit_pub_update.update(cursor=cursor, new_lit_pub=pub)
                else:
                    data_pub_update.update(cursor=cursor, new_data_pub=pub)

            connection.commit()
