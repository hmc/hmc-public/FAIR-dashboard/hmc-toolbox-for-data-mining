SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;

CREATE TABLE `publication` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL DEFAULT 'Literature',
  `title` varchar(2048) DEFAULT NULL,
  `journal` varchar(512) DEFAULT NULL,
  `publisher_deprecated` varchar(512) NOT NULL,
  `publication_year` int(11) DEFAULT NULL,
  `internal_id` varchar(45) DEFAULT NULL,
  `internal_id_type` varchar(45) DEFAULT NULL,
  `last_updated` date NOT NULL,
  `publication_date` varchar(10) DEFAULT NULL,
  `publisher_list` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `year_idx` (`publication_year`),
  KEY `title_idx` (`title`(1024)),
  KEY `journal_idx` (`journal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `center` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `center_has_publication` (
  `center_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`center_id`,`publication_id`),
  KEY `fk_center_has_publication_publication1_idx` (`publication_id`),
  KEY `fk_center_has_publication_center1_idx` (`center_id`),
  CONSTRAINT `fk_center_has_publication_center1` FOREIGN KEY (`center_id`) REFERENCES `center` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_center_has_publication_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `fuji_score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publication_id` int(10) unsigned NOT NULL,
  `metric_specification` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL,
  `maturity_A` float(5,2) NOT NULL,
  `maturity_A1` float(5,2) NOT NULL,
  `maturity_F` float(5,2) NOT NULL,
  `maturity_F1` float(5,2) NOT NULL,
  `maturity_F2` float(5,2) NOT NULL,
  `maturity_F3` float(5,2) NOT NULL,
  `maturity_F4` float(5,2) NOT NULL,
  `maturity_FAIR` float(5,2) NOT NULL,
  `maturity_I` float(5,2) NOT NULL,
  `maturity_I1` float(5,2) NOT NULL,
  `maturity_I2` float(5,2) NOT NULL,
  `maturity_I3` float(5,2) NOT NULL,
  `maturity_R` float(5,2) NOT NULL,
  `maturity_R1` float(5,2) NOT NULL,
  `maturity_R1.1` float(5,2) NOT NULL,
  `maturity_R1.2` float(5,2) NOT NULL,
  `maturity_R1.3` float(5,2) NOT NULL,
  `score_earned_A` float(5,2) NOT NULL,
  `score_earned_A1` float(5,2) NOT NULL,
  `score_earned_F` float(5,2) NOT NULL,
  `score_earned_F1` float(5,2) NOT NULL,
  `score_earned_F2` float(5,2) NOT NULL,
  `score_earned_F3` float(5,2) NOT NULL,
  `score_earned_F4` float(5,2) NOT NULL,
  `score_earned_FAIR` float(5,2) NOT NULL,
  `score_earned_I` float(5,2) NOT NULL,
  `score_earned_I1` float(5,2) NOT NULL,
  `score_earned_I2` float(5,2) NOT NULL,
  `score_earned_I3` float(5,2) NOT NULL,
  `score_earned_R` float(5,2) NOT NULL,
  `score_earned_R1` float(5,2) NOT NULL,
  `score_earned_R1.1` float(5,2) NOT NULL,
  `score_earned_R1.2` float(5,2) NOT NULL,
  `score_earned_R1.3` float(5,2) NOT NULL,
  `score_percent_A` float(5,2) NOT NULL,
  `score_percent_A1` float(5,2) NOT NULL,
  `score_percent_F` float(5,2) NOT NULL,
  `score_percent_F1` float(5,2) NOT NULL,
  `score_percent_F2` float(5,2) NOT NULL,
  `score_percent_F3` float(5,2) NOT NULL,
  `score_percent_F4` float(5,2) NOT NULL,
  `score_percent_FAIR` float(5,2) NOT NULL,
  `score_percent_I` float(5,2) NOT NULL,
  `score_percent_I1` float(5,2) NOT NULL,
  `score_percent_I2` float(5,2) NOT NULL,
  `score_percent_I3` float(5,2) NOT NULL,
  `score_percent_R` float(5,2) NOT NULL,
  `score_percent_R1` float(5,2) NOT NULL,
  `score_percent_R1.1` float(5,2) NOT NULL,
  `score_percent_R1.2` float(5,2) NOT NULL,
  `score_percent_R1.3` float(5,2) NOT NULL,
  `score_total_A` float(5,2) NOT NULL,
  `score_total_A1` float(5,2) NOT NULL,
  `score_total_F` float(5,2) NOT NULL,
  `score_total_F1` float(5,2) NOT NULL,
  `score_total_F2` float(5,2) NOT NULL,
  `score_total_F3` float(5,2) NOT NULL,
  `score_total_F4` float(5,2) NOT NULL,
  `score_total_FAIR` float(5,2) NOT NULL,
  `score_total_I` float(5,2) NOT NULL,
  `score_total_I1` float(5,2) NOT NULL,
  `score_total_I2` float(5,2) NOT NULL,
  `score_total_I3` float(5,2) NOT NULL,
  `score_total_R` float(5,2) NOT NULL,
  `score_total_R1` float(5,2) NOT NULL,
  `score_total_R1.1` float(5,2) NOT NULL,
  `score_total_R1.2` float(5,2) NOT NULL,
  `score_total_R1.3` float(5,2) NOT NULL,
  `status_passed_A` float(5,2) NOT NULL,
  `status_passed_A1` float(5,2) NOT NULL,
  `status_passed_F` float(5,2) NOT NULL,
  `status_passed_F1` float(5,2) NOT NULL,
  `status_passed_F2` float(5,2) NOT NULL,
  `status_passed_F3` float(5,2) NOT NULL,
  `status_passed_F4` float(5,2) NOT NULL,
  `status_passed_FAIR` float(5,2) NOT NULL,
  `status_passed_I` float(5,2) NOT NULL,
  `status_passed_I1` float(5,2) NOT NULL,
  `status_passed_I2` float(5,2) NOT NULL,
  `status_passed_I3` float(5,2) NOT NULL,
  `status_passed_R` float(5,2) NOT NULL,
  `status_passed_R1` float(5,2) NOT NULL,
  `status_passed_R1.1` float(5,2) NOT NULL,
  `status_passed_R1.2` float(5,2) NOT NULL,
  `status_passed_R1.3` float(5,2) NOT NULL,
  `status_total_A` float(5,2) NOT NULL,
  `status_total_A1` float(5,2) NOT NULL,
  `status_total_F` float(5,2) NOT NULL,
  `status_total_F1` float(5,2) NOT NULL,
  `status_total_F2` float(5,2) NOT NULL,
  `status_total_F3` float(5,2) NOT NULL,
  `status_total_F4` float(5,2) NOT NULL,
  `status_total_FAIR` float(5,2) NOT NULL,
  `status_total_I` float(5,2) NOT NULL,
  `status_total_I1` float(5,2) NOT NULL,
  `status_total_I2` float(5,2) NOT NULL,
  `status_total_I3` float(5,2) NOT NULL,
  `status_total_R` float(5,2) NOT NULL,
  `status_total_R1` float(5,2) NOT NULL,
  `status_total_R1.1` float(5,2) NOT NULL,
  `status_total_R1.2` float(5,2) NOT NULL,
  `status_total_R1.3` float(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fuji_score_publication1` (`publication_id`),
  CONSTRAINT `fk_fuji_score_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `instrument` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `instrument_has_publication` (
  `instrument_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`instrument_id`,`publication_id`),
  KEY `fk_instrument_has_publication_publication1_idx` (`publication_id`),
  KEY `fk_instrument_has_publication_instrument1_idx` (`instrument_id`),
  CONSTRAINT `fk_instrument_has_publication_instrument1` FOREIGN KEY (`instrument_id`) REFERENCES `instrument` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instrument_has_publication_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `large_scale_facility` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `large_scale_facility_has_publication` (
  `large_scale_facility_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`large_scale_facility_id`,`publication_id`),
  KEY `fk_large_scale_facility_has_publication_publication1_idx` (`publication_id`),
  KEY `fk_large_scale_facility_has_publication_large_scale_facilit_idx` (`large_scale_facility_id`),
  CONSTRAINT `fk_large_scale_facility_has_publication_large_scale_facility1` FOREIGN KEY (`large_scale_facility_id`) REFERENCES `large_scale_facility` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_large_scale_facility_has_publication_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `metric` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publication_id` int(10) unsigned NOT NULL,
  `type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_metric_publication1_idx` (`publication_id`),
  CONSTRAINT `fk_metric_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `publication_authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_names` longtext NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_publication_authors_publication1_idx` (`publication_id`),
  CONSTRAINT `fk_publication_authors_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `publication_identifier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publication_id` int(10) unsigned NOT NULL,
  `identifier_id` varchar(128) NOT NULL,
  `identifier_type` varchar(45) NOT NULL,
  `identifier_url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_id_UNIQUE` (`identifier_id`,`identifier_type`),
  KEY `fk_publication_identifier_publication1_idx` (`publication_id`),
  CONSTRAINT `fk_publication_identifier_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `reference` (
  `type` varchar(45) DEFAULT NULL,
  `reference_id` int(10) unsigned NOT NULL,
  `reference_to_id` int(10) unsigned NOT NULL,
  `sub_type` varchar(45) DEFAULT NULL,
  `sub_type_schema` varchar(45) DEFAULT NULL,
  UNIQUE KEY `reference_pk` (`type`,`reference_id`,`reference_to_id`,`sub_type`),
  KEY `fk_reference_publication1_idx` (`reference_id`),
  KEY `fk_reference_publication2_idx` (`reference_to_id`),
  CONSTRAINT `fk_reference_publication1` FOREIGN KEY (`reference_id`) REFERENCES `publication` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_reference_publication2` FOREIGN KEY (`reference_to_id`) REFERENCES `publication` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `related_identifier` (
  `related_identifier` text DEFAULT NULL,
  `related_identifier_type` varchar(45) DEFAULT NULL,
  `relation_type` varchar(45) DEFAULT NULL,
  `resource_type` varchar(45) DEFAULT NULL,
  `publication_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE INDEX related_identifier_publication_id_index
    ON related_identifier (publication_id);

CREATE INDEX related_identifier_related_identifier_index
    ON related_identifier (related_identifier);

CREATE TABLE `research_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `research_field_has_publication` (
  `research_field_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`research_field_id`,`publication_id`),
  KEY `fk_research_field_has_publication_publication1_idx` (`publication_id`),
  KEY `fk_research_field_has_publication_research_field1_idx` (`research_field_id`),
  CONSTRAINT `fk_research_field_has_publication_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_research_field_has_publication_research_field1` FOREIGN KEY (`research_field_id`) REFERENCES `research_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `research_sub_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `research_sub_field_has_publication` (
  `research_sub_field_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`research_sub_field_id`,`publication_id`),
  KEY `fk_research_sub_field_has_publication_publication1_idx` (`publication_id`),
  KEY `fk_research_sub_field_has_publication_research_sub_field1_idx` (`research_sub_field_id`),
  CONSTRAINT `fk_research_sub_field_has_publication_publication1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_research_sub_field_has_publication_research_sub_field1` FOREIGN KEY (`research_sub_field_id`) REFERENCES `research_sub_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
